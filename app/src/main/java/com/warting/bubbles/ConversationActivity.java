package com.warting.bubbles;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ListView;

import com.trios.shipdestoyer.EndGameWin;
import com.trios.shipdestoyer.R;

import java.util.ArrayList;


public class ConversationActivity extends Activity
{
    private static int convo;
    private static ArrayList< OneComment > convos = new ArrayList<>();
    private static boolean post = false;
    private com.warting.bubbles.DiscussArrayAdapter adapter;
    private ListView lv;

    public static void setConvoOnePre()
    {
        post = false;
        convos.clear();
        convos.add( new OneComment( 'L', "Morning Mav, Fleet Admiral Sparrow reporting in. So glad we have you for this operation Mav." ) );
        convos.add( new OneComment( 'R', "Aye Cap, you can always count on me for a do-or-die suicide mission to save humanity. Does wonders for my ego." ) );

        convos.add( new OneComment( 'L', "Witty. Okay, we tracked a lone enemy ship to an outlying system, perfect for testing out The Ship and your ability to adapt. " ) );

        convos.add( new OneComment( 'R', "The ol’ mettle vs. metal eh Cap." ) );

        convos.add( new OneComment( 'L', "Eloquently put. Ok. The Ship is all about defense. " +
                "Our standard weapons haven’t been effective, and so me and my team developed a prototype spacecraft that uses the Daughter’s weapons against them. " +
                "Using forcefield energy The Ship reflects the enemy projectiles back at them!" ) );
        convos.add( new OneComment( 'R', "Those are some cowboy tactics Cap, I’m comin’ to dig this side of you. Sittin’ in the hotseat it all looks familiar, I can jam to this." ) );
        convos.add( new OneComment( 'L', "Ok Mav, here’s a crash course in The Ship’s mechanics. " +
                "You have four shields, activating them emits a defense forcefield. " +
                "The Daughter’s weapons will be raining upon you Mav, you can use the shields to deflect the projectiles back at them." +
                " If you hold the shield upon activating you can absorb the projectile, reducing the shields stability. " +
                "That stability restores over time however, so manage well Mav. Any questions?" ) );

        convos.add( new OneComment( 'R', "Nah, you look like you got it all Cap." ) );
        convos.add( new OneComment( 'L', "Besides shield stability, you need to keep in mind that The Ship isn’t invincible." +
                "Failing to reflect or absorb attacks will result in damage to the hull and core systems. " +
                "Increased damage will result in critical failure." ) );
        convos.add( new OneComment( 'R', "You act like I’ve never flown before Birdy, there’s a reason you chose me for this suicide mission. " +
                "Besides my refusal to fear death." ) );
        convos.add( new OneComment( 'L', "I know but be cautious Mav, the Daughter’s are bound to have some surprises." ) );
        convos.add( new OneComment( 'R', "Excellent foreshadowing Cap, let’s hope your wrong. " ) );
        //The first mission involves a skirmish with a rogue battleship that broke off from the main fleet, an
        //easy mission more so used to get Cap familiar with the prototype Ship.
    }

    public static void setConvoOnePost()
    {
        post = true;
        //Dialogue begins after the enemy Daughter has been destroyed.
        convos.clear();
        convos.add( new OneComment( 'R', "Burn baby, burn!" ) );

        convos.add( new OneComment( 'L', "Mission success, excellent work Mav. I have to profess, I am impressed." ) );

        convos.add( new OneComment( 'R', "You keep talkin’ like that Cap and I’m turning this girl around." ) );

        convos.add( new OneComment( 'L', "Ten-four. Your next mission is trickier, there’s a battalion positioned to close for comfort." ) );
        convos.add( new OneComment( 'L', "The leader is a powerful ship, should be no match for your skills though." ) );

        convos.add( new OneComment( 'R', "I’m a single man Cap, ain’t nothin’ a match for me." ) );

        convos.add( new OneComment( 'L', "Charming wit Mav." ) );

        convos.add( new OneComment( 'R', "We got a beautiful friendship going here Cap, don’t ruin it." ) );
    }

    public static void setConvoTwoPre()
    {
        post = false;
        convos.clear();
        convos.add( new OneComment( 'L', "Our scouting software has picked up increased weaponry present on your next target Mav. " ) );
        convos.add( new OneComment( 'R', "Nothing I can't handle Cap, I'm simply the best." ) );
        convos.add( new OneComment( 'L', "Use that confidence Mav, we need you running at a constant hundred percent." ) );
        convos.add( new OneComment( 'R', "Thanks for the advice Dr. Birdbrain, I ain’t payin’ for this though." ) );
        convos.add( new OneComment( 'L', "Let’s hope you don’t." ) );
    }

    public static void setConvoTwoPost()
    {
        post = true;
        convos.clear();
        convos.add( new OneComment( 'R', "Oh baby! That was a one-two punch right in the jaw! " +
                "These shields of yours are some kinda wonder Cap." ) );
        convos.add( new OneComment( 'L', "Thanks Mav, you’re proving their effectiveness quite well. " +
                "We are gathering data for the next engagement. " +
                "Maintain your nutrients with the provided onboard sustenance." ) );
        convos.add( new OneComment( 'R', "Where did ya hide the Skittles Cap?" ) );
        convos.add( new OneComment( 'L', "Our quartermaster says he never received that order Mav, I apologize.." ) );
        convos.add( new OneComment( 'R', "..." ) );
        convos.add( new OneComment( 'R', "I see." ) );
        convos.add( new OneComment( 'L', "Anger will cause errors in judgement Mav, please...don’t be mad." ) );
        convos.add( new OneComment( 'R', "I’m not mad Cap, just...disappointed." ) );
    }

    public static void setConvoThreePre()
    {
        post = false;
        convos.clear();
        convos.add( new OneComment( 'L', "Our reports indicate a large enemy presence near your position and ready to attack." ) );
        convos.add( new OneComment( 'R', "I can handle the big girl." ) );
        convos.add( new OneComment( 'L', "Make sure your destruction is total. Not a fragment of these Daughters must remain." ) );
        convos.add( new OneComment( 'C', "***STATIC INTERFERENCE***" ) );
        convos.add( new OneComment( 'C', "Why must you struggle so?...accept our embrace...my children...are crying..." ) );
        convos.add( new OneComment( 'C', "***STATIC INTERFERENCE***" ) );
        convos.add( new OneComment( 'L', "Mav! Respond!" ) );
        convos.add( new OneComment( 'R', "What the hell was that Cap?" ) );
        convos.add( new OneComment( 'L', "Unsure Mav, we lost transmission with you. " +
                "Our radios were emitting a high pitched frequency, it hit the cabin like a flashbang. " +
                "Are your instruments ok? Are they reading any damage?" ) );
        convos.add( new OneComment( 'R', "Everything’s fine physically, but there was a woman Cap...talking over the radio. " +
                "She sounded...familiar…" ) );
        convos.add( new OneComment( 'L', "Hmm. The Daughter’s may be initializing a defensive tactic we haven’t planned for. " ) );
        convos.add( new OneComment( 'R', "Nothing about this is really planned Cap, but I might lose my mind before the end of it." ) );
    }

    public static void setConvoThreePost()
    {
        post = true;
        convos.clear();
        convos.add( new OneComment( 'R', "Reporting mission success. Whew! That was a helluva doozy!" ) );
        convos.add( new OneComment( 'L', "As suspected, the enemy is instituting a variety of weaponry. " +
                "Mav, your balancing is excellent and timing impeccable. " +
                "I can only say, stay sharp." ) );
        convos.add( new OneComment( 'R', "Aye Cap, am I just gonna shoot through ships until the universe ends though?" +
                " My debrief was unprofessionally short." ) );
        convos.add( new OneComment( 'L', "..." ) );
        convos.add( new OneComment( 'L', "You refused to attend the debriefing. " ) );
        convos.add( new OneComment( 'R', "..." ) );
        convos.add( new OneComment( 'R', "True." ) );
        convos.add( new OneComment( 'L', "But to answer your question, no Mav. There’s an endgame. " +
                "The Daughter’s formation resembles that of a giant spiral, requiring multiple points of attack to reach the centre. " +
                "However, there IS a centre. " +
                "Something controls these ships, and some bigger thing controls that. We have to eliminate it." +
                " Humanity has suffered long enough at the hands of the Daughters." ) );
        convos.add( new OneComment( 'R', "I’ll take ‘em down Cap. I’m a bullet, going straight for the heart." ) );
    }


    public static void setConvoFourPre()
    {
        post = false;
        convos.clear();
        convos.add( new OneComment( 'L', "Our remote sensory data shows The Ship’s hull and core systems at full operational capacity. " +
                "I think it’s time we push forward, your skills will be tested Mav. " +
                "The Daughter’s ability to adapt is well known." ) );
        convos.add( new OneComment( 'R', "This baby is clearing ‘em out real nice though Cap! I’m jammin’ to this real good. Full throttle all the way." ) );
        convos.add( new OneComment( 'C', "***STATIC INTERFERENCE***" ) );
        convos.add( new OneComment( 'C', " Do not continue...you will know only death...and pain...please...the pain…" ) );
        convos.add( new OneComment( 'C', "..." ) );
        convos.add( new OneComment( 'C', "please...Mav…" ) );
        convos.add( new OneComment( 'C', "***STATIC INTERFERENCE***" ) );
        convos.add( new OneComment( 'L', "Mav?!" ) );
        convos.add( new OneComment( 'R', "Aye, Birdy. I’m here. And I’m mad. " +
                "What the hell is going on with this thing you got me in. " +
                "It’s all outta order." ) );
        convos.add( new OneComment( 'L', "This is something we have never come across Mav, nothing appears to be functionally wrong. " +
                "Interference is being caused from...somewhere." ) );
        convos.add( new OneComment( 'R', "Or some thing Cap, it was talkin’ to me. And...it knew my name." ) );
        convos.add( new OneComment( 'L', "Who did?" ) );
        convos.add( new OneComment( 'R', "The voice...over the radio Cap. I sound crazy goddamn...startin’ to lose it." ) );
        convos.add( new OneComment( 'L', "Unfortunately, I can’t let you turn around. The damage you’re causing to the Daughters is invaluable. " +
                "We will continue to monitor the cause of this. Let’s push on." ) );
        convos.add( new OneComment( 'R', "You’re a real go getter Cap." ) );
    }

    public static void setConvoFourPost()
    {
        post = true;
        convos.clear();
        convos.add( new OneComment( 'L', "Status. Mission success. Excellent Mav. " +
                "Let’s not waste a moment, the next target is in immediate vicinity. " ) );
        convos.add( new OneComment( 'R', "I like when you get aggressive Cap, oh baby! Let’s get ‘er!" ) );
    }


    public static void setConvoFivePre()
    {
        post = false;
        convos.clear();
        convos.add( new OneComment( 'L', "Incoming transmission, Mav?" ) );
        convos.add( new OneComment( 'L', "..." ) );
        convos.add( new OneComment( 'L', "Mav! We require immediate response!" ) );
        convos.add( new OneComment( 'R', "Yeah yeah yeah, I’m here Cap. " +
                "Give a man some time to relieve himself for chrissakes, I’m not a robopilot. " +
                "I ain’t got gears in me like you Sparrow." ) );
        convos.add( new OneComment( 'L', "I’m a cyborg Mav, half-robot." ) );
        convos.add( new OneComment( 'R', "Whatever helps you sleep better Cap, you got my mission specs?" ) );
        convos.add( new OneComment( 'L', "Our discovery system is showing a fleet moving in, a group of ships." ) );
        convos.add( new OneComment( 'R', "Aye, the girls like to stay together. " +
                "Wonder what’ll happen if I ask one to dance?" ) );
        convos.add( new OneComment( 'L', "If you stay off her toes and on yours, well Mav...you’ll do just fine." ) );
        convos.add( new OneComment( 'R', "I’m gonna have to remember that one Cap." ) );
    }

    public static void setConvoFivePost()
    {
        post = true;
        convos.clear();
        convos.add( new OneComment( 'L', "Wow Mav, you’re proving to be the best choice for this operation." ) );
        convos.add( new OneComment( 'R', "Did ya really have any doubts though?" ) );
        convos.add( new OneComment( 'C', "***STATIC INTERFERENCE***" ) );
        convos.add( new OneComment( 'C', "My Daughters...lie in pieces...at your feet…" ) );
        convos.add( new OneComment( 'C', "..." ) );
        convos.add( new OneComment( 'C', "Retreat...I can’t….destroy...Mav...my…" ) );
        convos.add( new OneComment( 'C', "***STATIC INTERFERENCE***" ) );
        convos.add( new OneComment( 'L', "Mav! Our sensors are setting off every alert they have, status! Now!" ) );
        convos.add( new OneComment( 'R', "It’s all fine Cap! You tellin’ me you ain’t hearin’ this?!" ) );
        convos.add( new OneComment( 'L', "These communication blackouts are definitely being caused by the Daughters. " +
                " It doesn’t appear to be any sort of jamming technology we’ve encountered before." ) );
        convos.add( new OneComment( 'R', "It’s giving me a damn migraine Birdy, I’m shutting you off." ) );
        convos.add( new OneComment( 'R', "***SIGNAL LOST***" ) );
    }


    public static void setConvoSixPre()
    {
        post = false;
        convos.clear();
        convos.add( new OneComment( 'L', "Mav, we’ve uploaded the mission specs to your Ship. Respond with receipt confirmation." ) );
        convos.add( new OneComment( 'L', "..." ) );
        convos.add( new OneComment( 'L', "Mav? Have your received the specs?" ) );
        convos.add( new OneComment( 'L', "..." ) );
        convos.add( new OneComment( 'L', "Mav?" ) );
        convos.add( new OneComment( 'L', "Goddamnit." ) );
    }

    public static void setConvoSixPost()
    {
        post = true;
        convos.clear();
        convos.add( new OneComment( 'L', "Mav, we are reporting mission success. Going in blind is against protocol." +
                " We can’t succeed in this if you make your own rules." +
                " I need a response from you Mav." ) );
        convos.add( new OneComment( 'C', "***STATIC INTERFERENCE***" ) );
        convos.add( new OneComment( 'C', "You...will lose...Mav...to...us…" ) );
        convos.add( new OneComment( 'L', " Identify yourself." ) );
        convos.add( new OneComment( 'C', "He...will...love us...again…" ) );
        convos.add( new OneComment( 'L', "How are you intercepting this protected channel? " +
                "We are zeroing in on your location and the source of this transmission. " +
                "Who are you?" ) );
        convos.add( new OneComment( 'C', "I...am...the end…" ) );
        convos.add( new OneComment( 'C', "..." ) );
        convos.add( new OneComment( 'C', "...of everything." ) );
        convos.add( new OneComment( 'C', "***STATIC INTERFERENCE***" ) );
    }

    public static void setConvoSevenPre()
    {
        post = false;
        convos.clear();
        convos.add( new OneComment( 'R', "Aye, Cap. You there?" ) );
        convos.add( new OneComment( 'L', "Mav! Your abuse of protocol is unwarranted. " +
                "We were almost forced to end this operation as a failure." ) );
        convos.add( new OneComment( 'R', "Sorry brotha. Just needed some time to clear my head. Carnage without care. My kinda therapy." ) );
        convos.add( new OneComment( 'L', "We had a rogue transmission from your radio in your absence..." ) );
        convos.add( new OneComment( 'R', "Oh, I heard it. Part of the reason I’m ringing you up. " +
                "Well, that and I need mission details or whatever. " +
                "Who is this trying to ruin our day?" ) );
        convos.add( new OneComment( 'L', "Our scanners traced the source to the centre of the Daughters formation." ) );
        convos.add( new OneComment( 'R', "...Mother." ) );
        convos.add( new OneComment( 'L', "As the grunts call it, yes. Mother. " +
                "Something is emitting this...broadcast. For reasons we haven’t be able to ascertain." +
                " But somehow...I think...it knows you." ) );
        convos.add( new OneComment( 'R', "Yeah. And it’s eating me up inside. " +
                "Something about it feels familiar...but...wrong. " +
                "In psych class they called it the Cosby Effect. Some condition from the last century. " +
                "Cap, just tell me it’ll all be okay." ) );
        convos.add( new OneComment( 'L', "I don’t make promises I can’t keep." ) );
        convos.add( new OneComment( 'R', " Awh Cap, just once can’t you lie to me?" ) );
        convos.add( new OneComment( 'L', " Mission is a go Mav, proceed." ) );
    }

    public static void setConvoSevenPost()
    {
        post = true;
        convos.clear();
        convos.add( new OneComment( 'R', "Damn Cap! These girls are getting tougher!" ) );
        convos.add( new OneComment( 'L', "It would be poor war tactics if they became weaker." ) );
        convos.add( new OneComment( 'R', "What happened to the jokes Birdy, you’re bummin’ me out. " +
                "And I’m the one alone in space on a suicide mission." ) );
        convos.add( new OneComment( 'L', "Sorry Mav. That rogue transmission unsettled me greatly. " +
                "I do not like thinking the enemy has information I do not expect." ) );
        convos.add( new OneComment( 'R', "Either way I’ll be blowing ‘em up, stop worrying." ) );
        convos.add( new OneComment( 'L', "I’ll stop worrying when this is over." ) );
        convos.add( new OneComment( 'R', "So when I’m dead? Jesus Cap." ) );
        convos.add( new OneComment( 'L', "I just can never win, can I?" ) );
        convos.add( new OneComment( 'R', "Nope." ) );
    }


    public static void setConvoEightPre()
    {
        post = false;
        convos.clear();
        convos.add( new OneComment( 'L', "Mav, your performance is akin to machine. Your response time is on par and your tactics are excellent." ) );
        convos.add( new OneComment( 'R', "Thanks Cap, but it’s gonna take more than compliments to swoon this gal. " ) );
        convos.add( new OneComment( 'L', "Human resources told me I was too cold, I’m trying to show I'm proud of you." ) );
        convos.add( new OneComment( 'R', "What a strange moment to turn a new leaf Cap, are you also gonna go vegan? " ) );
        convos.add( new OneComment( 'L', "Everyone is a vegan Mav. " ) );
        convos.add( new OneComment( 'R', "I know, it’s just a saying. " +
                "We ready to rock and roll or are we gonna chit chat ‘til the Sun goes dark? " ) );
        convos.add( new OneComment( 'L', "Proceed when ready." ) );
    }

    public static void setConvoEightPost()
    {
        post = true;
        convos.clear();
        convos.add( new OneComment( 'R', "Woo Cap! Got ‘em real good!" ) );
        convos.add( new OneComment( 'R', "..." ) );
        convos.add( new OneComment( 'R', "Cap?" ) );
        convos.add( new OneComment( 'C', "***STATIC INTERFERENCE***" ) );
        convos.add( new OneComment( 'C', "Mav...we...are alone…" ) );
        convos.add( new OneComment( 'R', "We? Who the hell are you! " ) );
        convos.add( new OneComment( 'C', "Please Mav...why...must you...forget…" ) );
        convos.add( new OneComment( 'R', " Forget what? We know it’s you Mother! " +
                "I ain’t stoppin’ ‘til you’re a supernova." ) );
        convos.add( new OneComment( 'C', "Why...must you...forget me…" ) );
        convos.add( new OneComment( 'R', " I don’t know you!" ) );
        convos.add( new OneComment( 'C', "Why...do you fight...this war…" ) );
        convos.add( new OneComment( 'R', "..." ) );
        convos.add( new OneComment( 'R', "You better shut the hell up. I can't turn this off! what are you?!" ) );
        convos.add( new OneComment( 'C', "Please...remember..." ) );
        convos.add( new OneComment( 'C', "***STATIC INTERFERENCE***" ) );
    }


    public static void setConvoNinePre()
    {
        post = false;
        convos.clear();
        convos.add( new OneComment( 'L', "Mav. Please respond." ) );
        convos.add( new OneComment( 'R', "Here, Cap." ) );
        convos.add( new OneComment( 'L', "Thank goodness Mav, we had another blackout." ) );
        convos.add( new OneComment( 'R', "Tell me about it. Shit’s gettin’ weird Cap. " +
                "I think...they’re tryna get to me." ) );
        convos.add( new OneComment( 'L', "Who is?" ) );
        convos.add( new OneComment( 'R', "Mother..or...the Daughters..or..I don’t know Cap but this reeks of something fishy." ) );
        convos.add( new OneComment( 'L', "Well, let’s bring the aggression to the frontline and hit the next group hard. " +
                "The Ship’s readouts are all checking positive for stability. " +
                "You’re the farthest any human has gotten into their ranks Mav, they are sure to up their defensive strategies." +
                " Be on alert for changes in their tactics." ) );
        convos.add( new OneComment( 'R', "If at this point you still don’t trust me Cap, our relationship is hopeless. " +
                "And to think I had our honeymoon planned and everything." ) );
        convos.add( new OneComment( 'L', "Maybe you are losing it." ) );
    }

    public static void setConvoNinePost()
    {
        post = true;
        convos.clear();
        convos.add( new OneComment( 'R', "Reporting mission success." ) );
        convos.add( new OneComment( 'L', "Whoa, actually following protocol Mav. This is new." ) );
        convos.add( new OneComment( 'R', "Just accept it Cap, no need to point everything out." ) );
        convos.add( new OneComment( 'L', "Mav? I have a question. The last rogue transmission, it was received by us as well." ) );
        convos.add( new OneComment( 'R', "I see." ) );
        convos.add( new OneComment( 'L', "It had me thinking, and please tell me if I’ve crossed a line, but...why DID you join the war Mav?" ) );
        convos.add( new OneComment( 'R', "..." ) );
        convos.add( new OneComment( 'R', "You crossed a line Cap. Moving to next target, let’s light ‘em up." ) );
    }

    public static void setConvoTenPre()
    {
        post = false;
        convos.clear();
        convos.add( new OneComment( 'L', "I dislike getting my hopes up, but you are increasingly getting closer to the centre of their formation. " +
                "Every battle I’m on the edge of my seat. " +
                "However, I can’t deny your rate of success." ) );
        convos.add( new OneComment( 'R', "I’ve been thinkin’ about writing a book after all this is done. " +
                "“Success on the Front Line: The Tale of How One Man Saved Humanity by Mav E. Rick”." ) );
        convos.add( new OneComment( 'L', "Modest." ) );
        convos.add( new OneComment( 'R', "Never learned modesty Cap, some say it’s a flaw. I say I’m flawless. Tomato, tomahtoe." ) );
        convos.add( new OneComment( 'L', "If you survive this Mav, I’ll buy the first copy." ) );
        convos.add( new OneComment( 'R', "Literally the least you could do, eh Cap?" ) );
    }

    public static void setConvoTenPost()
    {
        post = true;
        convos.clear();
        convos.add( new OneComment( 'R', "Makin’ quick work of these little ugly ducklings. " +
                "I gotta say, I think I may keep this Ship for myself. As payment." ) );
        convos.add( new OneComment( 'L', "Sure Mav, your retirement fund." ) );
        convos.add( new OneComment( 'R', "Retirement is for people who give up." ) );
        convos.add( new OneComment( 'L', "Mav! We are noticing a spike in…" ) );
        convos.add( new OneComment( 'C', "***STATIC INTERFERENCE*** " ) );
        convos.add( new OneComment( 'C', "Mav...why...couldn’t you...save me…" ) );
        convos.add( new OneComment( 'R', "You crazy bitch! Who are you? Goddamn, I can’t kill you things fast enough!" ) );
        convos.add( new OneComment( 'C', "You...can...save us...now..." ) );
        convos.add( new OneComment( 'C', "..." ) );
        convos.add( new OneComment( 'C', "She...was….ours…" ) );
        convos.add( new OneComment( 'R', "..." ) );
        convos.add( new OneComment( 'R', "Sophie?" ) );
        convos.add( new OneComment( 'C', "***STATIC INTERFERENCE***" ) );
    }


    public static void setConvoElevenPre()
    {
        post = false;
        convos.clear();
        convos.add( new OneComment( 'L', "Mav, for humanity’s sake, tell me you’re still there." ) );
        convos.add( new OneComment( 'R', "Humanity is a lucky sonabitch then." ) );
        convos.add( new OneComment( 'L', "I’m afraid these blackouts aren’t going to stop. " +
                "I fear you are right, they are trying emotional warfare. These tactics are unconventional. " +
                "Your mind is your only weapon, and defense, against it." ) );
        convos.add( new OneComment( 'R', "Aye Cap, and she’s taking a beating. The Fleet didn’t have any trainin’ for this shit. " ) );
        convos.add( new OneComment( 'L', "The intense physical demands are supposed to instill proper mental conditioning, but these are dirty tactics. " +
                "We don’t train for those. If you’re having trouble Mav, I need to help." ) );
        convos.add( new OneComment( 'L', "..." ) );
        convos.add( new OneComment( 'L', "I must know who Mother is referring too. Mav...who...is Sophie?" ) );
        convos.add( new OneComment( 'R', "Watch it Cap, the ice is perilously thin. " ) );
        convos.add( new OneComment( 'L', "We need to know everything, in order to structure a proper defense." ) );
        convos.add( new OneComment( 'R', "Why did you join the war Sparrow?" ) );
        convos.add( new OneComment( 'L', "You know why Mav." ) );
        convos.add( new OneComment( 'R', "Aye, you were built for this. A few spare parts, put together in a lab and programmed to kill. " +
                "Some of us have different tales though Cap. " +
                "Some join because they want to protect those they love." ) );
        convos.add( new OneComment( 'R', "..." ) );
        convos.add( new OneComment( 'R', "And some join because they have nothing left to protect. " +
                "The Daughters took that away from me. So...I joined up. " +
                "I wanted to destroy them like they destroyed me." ) );
        convos.add( new OneComment( 'L', "I...didn’t know." ) );
        convos.add( new OneComment( 'R', "Yeah, I had someone on the inside burn my records. " +
                "I wanted to be a ghost. A spectre of malevolence." ) );
        convos.add( new OneComment( 'L', "How does Mother know this information, that baffles me. " +
                "Throughout this whole war, espionage has never been strategy put into action by either side." ) );
        convos.add( new OneComment( 'R', "If I knew that Cap, maybe I would be able to get some sleep. " +
                "On the eve of the first attack from the Daughters, the first night of the new century, Sophie was lost when a sector of Utopia was blown off. " +
                "She was thrown into space on a piece of burning metal. " ) );
        convos.add( new OneComment( 'R', "..." ) );
        convos.add( new OneComment( 'R', "It almost makes all of this poetic….almost." ) );
        convos.add( new OneComment( 'L', "Target is sighted for next mission. Proceed when ready." ) );
    }

    public static void setConvoElevenPost()
    {
        post = true;
        convos.clear();
        convos.add( new OneComment( 'R', "Hell yeah! Another one in the bag!" ) );
        convos.add( new OneComment( 'C', "***STATIC INTERFERENCE***" ) );
        convos.add( new OneComment( 'C', "WHY...MUST YOU...DESTROY...YOUR...CHILDREN…" ) );
        convos.add( new OneComment( 'R', "Your madness will not get to me! " +
                "I haven’t fought this far to fall to some fucked up floozy floutin’ foolishness. " +
                "I’ll burn you all!" ) );
        convos.add( new OneComment( 'C', "WHY...MUST YOU...DESTROY..WHAT YOU...LOVE" ) );
        convos.add( new OneComment( 'R', "You took everything I love away from me!" ) );
        convos.add( new OneComment( 'C', "I’m not gone Mav...I’m just...different…" ) );
        convos.add( new OneComment( 'R', "Sophie?! Sophie? My god...Sophie...where are you..what..what is happening..." ) );
        convos.add( new OneComment( 'C', "They took me Mav...the Daughters...they made me their...Mother." ) );
        convos.add( new OneComment( 'R', "What? No! This can’t be true, no no no. None of this is true. Cap is right. You’re just afraid, you know I’m coming straight for your heart." ) );
        convos.add( new OneComment( 'C', "We don’t want to destroy you Mav, we want you to join us. " +
                "We need Father. We lost our child Mav, we can have children now." ) );
        convos.add( new OneComment( 'R', "They are ruthless killing machines! " +
                "They’ve punished humanity for over a decade for what? For nothing! " +
                "They crave our destruction, we can take it no more." ) );
        convos.add( new OneComment( 'C', "Please Mav...we can save you." ) );
        convos.add( new OneComment( 'R', "Like hell you can." ) );
    }


    public static void setConvoTwelvePre()
    {
        post = false;
        convos.clear();
        convos.add( new OneComment( 'L', "Communications restored. Mav, are you there?" ) );
        convos.add( new OneComment( 'R', "10-4 Cap" ) );
        convos.add( new OneComment( 'L', "Excellent. Our reports are showing one last battalion standing in the way of you and Mother. " +
                "I can’t believe you made it Mav, history will know your name." ) );
        convos.add( new OneComment( 'R', "I can’t do it Cap." ) );
        convos.add( new OneComment( 'L', "Sorry?" ) );
        convos.add( new OneComment( 'R', "Sophie...my...wife. She’s in there somehow Cap...she’s...part of them. " ) );
        convos.add( new OneComment( 'L', "Those transmissions are rogue Mav! It’s a psychological tactic, you can’t let it affect you. " ) );
        convos.add( new OneComment( 'R', "I heard her voice." ) );
        convos.add( new OneComment( 'L', "They are mimicking, trying to convince you." ) );
        convos.add( new OneComment( 'R', "How’d they know what she sounds like though Cap? " +
                "How does she know my name...my...history?" ) );
        convos.add( new OneComment( 'L', "I...I don’t know." ) );
        convos.add( new OneComment( 'R', "This is when I need answers Cap! I can’t...I can’t let her die again!" ) );
        convos.add( new OneComment( 'L', "So you will let humanity perish in her place?" ) );
        convos.add( new OneComment( 'R', "Fuck you Cap. You’re lucky I got a death wish." ) );
    }

    public static void setConvoTwelvePost()
    {
        post = true;
        convos.clear();
        convos.add( new OneComment( 'L', "Yes! We are pushing through Mav! We are on the brink of a new age and you are leading the charge. " ) );
        convos.add( new OneComment( 'R', "My inner turmoil is dampening my excitement." ) );
        convos.add( new OneComment( 'L', "We can’t stop now Mav, The Ship is on an automated course." ) );
        convos.add( new OneComment( 'R', "So I couldn’t even stop this if I wanted too?" ) );
        convos.add( new OneComment( 'L', "Sorry I didn’t mention anything Mav. We needed to ensure the operation was carried out. " +
                "The only unknown variable was your determination. Thankfully, that worked out." ) );
        convos.add( new OneComment( 'R', "Yeah. Thank God." ) );
    }


    public static void setConvoThirteenPre()
    {
        post = false;
        convos.clear();
        convos.add( new OneComment( 'L', "Your success is lighting up the universe Mav, truly a sight to behold." ) );
        convos.add( new OneComment( 'R', "You can cut the pleasantries Cap, I’m in it for the long run. " +
                "You’re right, I’m a fool to think humanity wasn’t worth it." ) );
        convos.add( new OneComment( 'C', "***STATIC INTERFERENCE***" ) );
        convos.add( new OneComment( 'C', "JOIN US...FATHER...JOIN US...FATHER" ) );
        convos.add( new OneComment( 'R', "No! Sophie...no. I can’t. Too many people have died at your hands." ) );
        convos.add( new OneComment( 'C', "Not you...we saved...you…" ) );
        convos.add( new OneComment( 'R', "I’m not worth saving, trust me. " +
                "When you died Sophie, I lost myself. " +
                "I had to pick up the pieces of my shattered existence and put them together. " +
                "I’ve changed. Not for the better." ) );
        convos.add( new OneComment( 'C', "Everything...can be...the same...as...it was…" ) );
        convos.add( new OneComment( 'R', "And live life knowing I doomed humanity because of a broken heart? " +
                "That just ain’t gonna fly with me. " ) );
        convos.add( new OneComment( 'C', "We will...give you...one...last chance...to...reconsider…" ) );
        convos.add( new OneComment( 'C', "***STATIC INTERFERENCE***" ) );
        convos.add( new OneComment( 'R', "No need, I’ve given up on your ghost." ) );
    }

    public static void setConvoThirteenPost()
    {
        post = true;
        convos.clear();
        convos.add( new OneComment( 'L', "That was absolutely fierce Mav! Total annihilation!" ) );
        convos.add( new OneComment( 'R', "The clouds are outta my head Cap, I’m seeing clear again." ) );
        convos.add( new OneComment( 'L', "It’s almost over Mav, we need you to see red." ) );
        convos.add( new OneComment( 'R', "Aye, let’s begin the final bloodbath." ) );
    }


    public static void setConvoFourteenPre()
    {
        post = false;
        convos.clear();
        convos.add( new OneComment( 'L', "Okay Mav, this is the last bastion of defense before you strike at the heart of Mother." ) );
        convos.add( new OneComment( 'R', "Come on Cap, just another day at the office. I’m reinvigorated and ready to dominate." ) );
        convos.add( new OneComment( 'L', "Your excitement is infectious Mav, I have a cold sweat and my legs are shaking. " +
                "This is an important moment for us all." ) );
        convos.add( new OneComment( 'R', "Soak it in Cap, take a picture. It’ll last longer." ) );

    }

    public static void setConvoFourteenPost()
    {
        post = true;
        convos.clear();
        convos.add( new OneComment( 'R', "Reporting mission success! I have visuals on Mother. " +
                "She’s a goddamn colossus!" ) );
        convos.add( new OneComment( 'R', "Cap?" ) );
        convos.add( new OneComment( 'R', "Sparrow?" ) );
        convos.add( new OneComment( 'C', "***STATIC INTERFERENCE***" ) );
        convos.add( new OneComment( 'C', "YOU..FORCE...MY HAND" ) );
        convos.add( new OneComment( 'R', "What the hell do you want, I already told you I ain’t being saved!" ) );
        convos.add( new OneComment( 'C', "THEN YOU...MAY DO SO...ALONE…" ) );
        convos.add( new OneComment( 'R', "What have you done with the Cap?" ) );
        convos.add( new OneComment( 'C', "YOU WILL...BOW...TO MY...REALITY…" ) );
        convos.add( new OneComment( 'R', "Even as an telepathic woman/alien hybrid you’re still playing games?! Jesus!" +
                " When will you things learn I am a force of reckoning." ) );
    }


    public static void setConvoFifteenPre()
    {
        post = false;
        convos.clear();
        convos.add( new OneComment( 'R', "Captain Sparrow, this is Mav. " +
                "I am unsure if my communications are being received but I have visuals on Mother and I am closing in for a final push. " +
                "May the Universe have mercy on my soul." ) );
        convos.add( new OneComment( 'C', "***STATIC INTERFERENCE***" ) );
        convos.add( new OneComment( 'C', "Please Mav...don’t do this...we love you…." ) );
        convos.add( new OneComment( 'R', "Love is something I gave up a long time ago. I know you’re not my wife, Sophie would never do this. " +
                "You’ve become a machine. Cold and callous. " +
                "I told Cap I was going down and taking you with me. I can’t disappoint him. " +
                "Goodbye Sophie." ) );
        convos.add( new OneComment( 'C', "THEN COME...MEET YOUR DEMISE." ) );
        convos.add( new OneComment( 'C', "***STATIC INTERFERENCE***" ) );
    }

    public static void setConvoFifteenPost()
    {
        post = true;
        convos.clear();
        convos.add( new OneComment( 'R', "Dine in hell swine! HAHAHA! " ) );
        convos.add( new OneComment( 'L', "Mav, please respond. Our instruments show positive readings on The Ship. " +
                "Mav, please respond." ) );
        convos.add( new OneComment( 'R', "Ah Cap! Good to hear your voice!" ) );
        convos.add( new OneComment( 'L', "Holy shit Mav, you did it! Mother is destroyed and the rest of the Daughters have begun to fall apart, disintegrating. " +
                "I can’t believe you actually did it...or maybe I can. " ) );
        convos.add( new OneComment( 'R', "Come on, you never had a doubt." ) );
        convos.add( new OneComment( 'L', "We will have a celebration of a lifetime for your return Mav." ) );
        convos.add( new OneComment( 'R', "Cap, I ain’t coming back." ) );
        convos.add( new OneComment( 'L', "What?" ) );
        convos.add( new OneComment( 'R', "Nothing for me on Utopia Cap, I kinda like the vast expanse of space. " +
                "I’ve takin’ a liking to thinking." ) );
        convos.add( new OneComment( 'L', "No no Mav, we need the Ship back. And everyone will want to know the man who saved everyone!" ) );
        convos.add( new OneComment( 'R', "The Daughters are gone Cap, you don’t need this thing. " +
                "Plus, I remember you saying I could keep it." ) );
        convos.add( new OneComment( 'L', "Mav that was-" ) );
        convos.add( new OneComment( 'R', "No no, that’s what I heard. Just...tell everyone I got lost. Who’s gonna say otherwise? " +
                "I think I’m gonna finish that book." ) );
        convos.add( new OneComment( 'L', "Come back Mav. Please." ) );
        convos.add( new OneComment( 'R', "Awh. I love ya Cap but the military life isn’t mine to lead anymore. " +
                "I think I’m gonna call this Ship the New Leaf." ) );
        convos.add( new OneComment( 'L', "I guess this is it then. Thank you for everything. Take care Mav." ) );
        convos.add( new OneComment( 'R', "Always do Cap." ) );
    }

    @Override
    public void onCreate( Bundle savedInstanceState )
    {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_discuss );

        lv = ( ListView ) findViewById( R.id.listView1 );
        convo = 0;
        adapter = new DiscussArrayAdapter( getApplicationContext(), R.layout.listitem_discuss );
        lv.setAdapter( adapter );
        adapter.add( convos.get( convo++ ) );
        lv.setOnTouchListener( new View.OnTouchListener()
        {
            @Override
            public boolean onTouch( View v, MotionEvent event )
            {
                // If the event is a key-down event on the "enter" button
                if ( ( event.getAction() == MotionEvent.ACTION_DOWN ) )
                {
                    if ( convo < convos.size() )
                    {
                        adapter.add( convos.get( convo++ ) );
                        lv.smoothScrollToPosition( convo );
                        return true;
                    }
                    else if ( !post )
                    {
                        finish();
                    }
                    else
                    {
                        Intent i = new Intent( v.getContext(), EndGameWin.class );
                        startActivity( i );
                        finish();
                    }
                }
                return false;
            }
        } );
    }
}