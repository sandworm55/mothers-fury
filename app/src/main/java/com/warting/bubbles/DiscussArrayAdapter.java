package com.warting.bubbles;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.trios.shipdestoyer.R;

import java.util.ArrayList;
import java.util.List;

public class DiscussArrayAdapter extends ArrayAdapter< OneComment >
{

    private TextView countryName;
    private List< OneComment > countries = new ArrayList< OneComment >();
    private LinearLayout wrapper;

    public DiscussArrayAdapter( Context context, int textViewResourceId )
    {
        super( context, textViewResourceId );
    }

    @Override
    public void add( OneComment object )
    {
        countries.add( object );
        super.add( object );
    }

    public int getCount()
    {
        return this.countries.size();
    }

    public OneComment getItem( int index )
    {
        return this.countries.get( index );
    }

    public View getView( int position, View convertView, ViewGroup parent )
    {
        View row = convertView;
        if ( row == null )
        {
            LayoutInflater inflater = ( LayoutInflater ) this.getContext().getSystemService( Context.LAYOUT_INFLATER_SERVICE );
            row = inflater.inflate( R.layout.listitem_discuss, parent, false );
        }

        wrapper = ( LinearLayout ) row.findViewById( R.id.wrapper );

        OneComment coment = getItem( position );

        countryName = ( TextView ) row.findViewById( R.id.comment );

        countryName.setText( coment.comment );

        switch ( coment.side )
        {
            case 'L':
                wrapper.setGravity( Gravity.LEFT );
                countryName.setBackgroundResource( R.drawable.bubble_yellow );
                break;
            case 'R':
                wrapper.setGravity( Gravity.RIGHT );
                countryName.setBackgroundResource( R.drawable.bubble_green );
                break;
            case 'C':
                wrapper.setGravity( Gravity.CENTER );
                countryName.setBackgroundResource( R.drawable.bubble_red );
                break;
        }
        return row;
    }

    public Bitmap decodeToBitmap( byte[] decodedByte )
    {
        return BitmapFactory.decodeByteArray( decodedByte, 0, decodedByte.length );
    }

}