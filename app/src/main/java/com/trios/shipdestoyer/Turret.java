package com.trios.shipdestoyer;

import com.trios.shipdestoyer.lib.SoundManager;
import com.trios.shipdestoyer.lib.Vector2D;

import java.util.ArrayList;

/**
 * Created by sandworm on 5/8/2017.
 */

public abstract class Turret implements GameObject
{
    private static ArrayList< Integer > streamIDs = new ArrayList<>();
    private static ArrayList< Integer > streamIDsToRemove = new ArrayList<>();
    protected float rotationSpeed = 0.3f, projectileSpeed = 2;//not final
    protected TurretTypes type;
    protected Vector2D pos;
    protected double facing;
    protected ShieldGenerator target;
    protected Vector2D lastTarget;
    protected ArrayList< Projectile > projectiles = new ArrayList<>();
    protected ArrayList< Projectile > projectilesToRemove = new ArrayList<>();
    protected ArrayList< HitEffect > hitEffects = new ArrayList<>();
    protected ArrayList< HitEffect > effectsToRemove = new ArrayList<>();
    protected ArrayList< Integer > offSet = new ArrayList<>();
    protected boolean lockedOn = false;
    protected int FIRE_RATE = 120;
    protected int currentFireRate = 0;
    protected int damage = 1;
    private int streamID;
    private boolean playingSound = false;
    private boolean playSound = false;
    private boolean onScreen = true;


    /**
     * @param type
     * @param facing
     */
    public Turret( TurretTypes type, int facing )
    {
        this.type = type;
        this.facing = facing;
    }


    @Override
    public void draw()
    {
        if ( pos == null )
            return;
        for ( Projectile projectile : projectiles )
            projectile.draw();

    }

    @Override
    public void update()
    {
        if ( pos != null && pos.getX() >= 0 && pos.getX() <= 480 )
        {
            onScreen = true;
            if ( currentFireRate++ >= FIRE_RATE )
                fireBullet();
        }
        else
        {
            onScreen = false;
        }

        rotateToTarget();
        soundLogic();

        for ( Projectile projectile : projectiles )
            projectile.update();
        for ( HitEffect eff : hitEffects )
            eff.update();

        for ( Projectile projectile : projectiles )
        {
            HitEffect hitEffect = projectile.hitTest();
            if ( hitEffect != null )
            {
                projectilesToRemove.add( projectile );
                hitEffects.add( hitEffect );
            }
        }

        for ( HitEffect eff : hitEffects )
            if ( eff.isDone() )
                effectsToRemove.add( eff );

        for ( Projectile projectile : projectilesToRemove )
            arrayClean( projectiles, projectile );
        for ( HitEffect eff : effectsToRemove )
            arrayClean( hitEffects, eff );
        projectilesToRemove.clear();
        effectsToRemove.clear();
    }

    /**
     * Adds a new projectile to the arraylist of projectiles, with a start point of this turret
     * and an end point of the turrets current target. This projectile uses attributes of this
     * turret for the type and speed of the round/beam/missile.
     */
    public Boolean fireBullet()
    {
        if ( !lockedOn )
            return false;

        currentFireRate = 0;
        projectiles.add( new Projectile( this.getPos(), target, projectileSpeed, this.getType(), this.damage ) );

        return true;
    }

    /**
     * if there is a different in the facing of the turret to what it should be, make a step towards
     * that current target, using the rotation speed of the turret to do so.
     */
    private void rotateToTarget()
    {
        lockedOn = false;

        if ( target != null )
        {
            double xDist = target.getPos().getX() - pos.getX();
            double yDist = target.getPos().getY() - pos.getY();
            double angle = Math.atan( xDist / -yDist ) * ( 180 / Math.PI );

            if ( Math.abs( facing - angle ) > rotationSpeed )
            {
                if ( facing < angle )
                {
                    facing += rotationSpeed;
                }
                else if ( facing > angle )
                {
                    facing -= rotationSpeed;
                }

                playSound = true;

            }
            else
            {
                lockedOn = true;
                facing = angle;

                playSound = false;
            }
        }
    }

    private void soundLogic()
    {
        if ( !lockedOn && !playingSound && playSound && onScreen )
        {
            //Turret turning - starting sound loop
            streamID = SoundManager.playSound( SoundManager.TURRET_TURNING, 0, -1, 1 );
            playingSound = true;
        }
        else if ( lockedOn && playingSound && !playSound )
        {
            //Turret locked on - stopping sound
            SoundManager.stopSound( streamID );
            playingSound = false;
        }
        else if ( !onScreen && playingSound )
        {
            //Turret offscreen - stopping sound
            SoundManager.stopSound( streamID );
            playingSound = false;
        }
    }

    private void arrayClean( ArrayList< Projectile > arr, Projectile projectile )
    {
        arr.remove( projectile );
    }

    private void arrayClean( ArrayList< HitEffect > arr, HitEffect effect )
    {
        arr.remove( effect );
    }


    public Vector2D getPos()
    {
        return pos;
    }

    public void setPos( Vector2D pos )
    {
        this.pos = pos;
    }

    public TurretTypes getType()
    {
        return type;
    }

    public ArrayList< HitEffect > getHitEffects()
    {
        return hitEffects;
    }

    public void setTarget( ShieldGenerator target )
    {
        if ( this.target == null )
            this.lastTarget = this.pos.add( new Vector2D( 0, 10 ) );
        else
            this.lastTarget = this.target.getPos();
        this.target = target;
    }
}
