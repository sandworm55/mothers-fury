package com.trios.shipdestoyer;

import com.trios.shipdestoyer.lib.Vector2D;

import java.util.ArrayList;

/**
 * Created by sandworm on 5/17/2017.
 */

public class LevelManager
{
    public static LevelNum currentLevel;

    public static void startLevel( Level level )
    {
        level.playing = true;
        level.levelEnded = false;
        level.levelWon = false;

        switch ( currentLevel )
        {
            case LEVEL_ONE:
                levelOne( level );
                break;
            case LEVEL_TWO:
                levelTwo( level );
                break;
            case LEVEL_THREE:
                levelThree( level );
                break;
            case LEVEL_FOUR:
                levelFour( level );
                break;
            case LEVEL_FIVE:
                levelFive( level );
                break;
            case LEVEL_SIX:
                levelSix( level );
                break;
            case LEVEL_SEVEN:
                levelSeven( level );
                break;
            case LEVEL_EIGHT:
                levelEight( level );
                break;
            case LEVEL_NINE:
                levelNine( level );
                break;
            case LEVEL_TEN:
                levelTen( level );
                break;
            case LEVEL_ELEVEN:
                levelEleven( level );
                break;
            case LEVEL_TWELVE:
                levelTwelve( level );
                break;
            case LEVEL_THIRTEEN:
                levelThirteen( level );
                break;
            case LEVEL_FOURTEEN:
                levelFourteen( level );
                break;
            case LEVEL_FIFTEEN:
                levelFifteen( level );
                break;
            default:
        }
    }

    public static void levelOne( Level level )
    {

        level.levelEnded = false;
        level.levelWon = false;
        level.levelType = 1;
        level.shipHealth = 5;

        ArrayList< Phase > phases = new ArrayList<>();
        phases.add( new Phase( new Vector2D( 240, 50 ), 1f ) );

        ArrayList< Turret > turrets = new ArrayList<>();
        turrets.add( new Ballistic( TurretTypes.BALLISTIC_SMALL, 0 ) );

        level.enemyShip = new EnemyShip( ShipTypes.PATROL, turrets, level.shipHealth, phases );

        setUpPlayer( level );

        level.objects.add( level.enemyShip );
        level.objects.add( level.playerShip );

        level.enemyShip.setTargets( 2, level.playerShip );

    }

    public static void levelTwo( Level level )
    {

        level.levelEnded = false;
        level.levelWon = false;
        level.levelType = 2;
        level.shipHealth = 20;

        ArrayList< Phase > phases = new ArrayList<>();
        phases.add( new Phase( new Vector2D( 240, 50 ), 1f ) );

        ArrayList< Turret > turrets = new ArrayList<>();
        turrets.add( new Ballistic( TurretTypes.BALLISTIC_SMALL, 0 ) );
        turrets.add( new Ballistic( TurretTypes.BALLISTIC_SMALL, 0 ) );
        turrets.add( new Ballistic( TurretTypes.BALLISTIC_SMALL, 0 ) );

        level.enemyShip = new EnemyShip( ShipTypes.GUN_DESTROYER, turrets, level.shipHealth, phases );

        setUpPlayer( level );

        level.objects.clear();
        level.objects.add( level.enemyShip );
        level.objects.add( level.playerShip );

        level.enemyShip.setTargets( 2, level.playerShip );
    }

    public static void levelThree( Level level )
    {

        level.levelEnded = false;
        level.levelWon = false;
        level.levelType = 3;
        level.shipHealth = 30;

        ArrayList< Phase > phases = new ArrayList<>();
        phases.add( new Phase( new Vector2D( 240, 50 ), 1f ) );

        ArrayList< Turret > turrets = new ArrayList<>();
        turrets.add( new Missile( TurretTypes.MINI_ROCKET, 0 ) );
        turrets.add( new Ballistic( TurretTypes.BALLISTIC_SMALL, 0 ) );
        turrets.add( new Ballistic( TurretTypes.BALLISTIC_SMALL, 0 ) );

        level.enemyShip = new EnemyShip( ShipTypes.CORVETTE, turrets, level.shipHealth, phases );

        setUpPlayer( level );

        level.objects.clear();
        level.objects.add( level.enemyShip );
        level.objects.add( level.playerShip );

        level.enemyShip.setTargets( 2, level.playerShip );
    }

    public static void levelFour( Level level )
    {

        level.levelEnded = false;
        level.levelWon = false;
        level.levelType = 4;
        level.shipHealth = 30;

        ArrayList< Phase > phases = new ArrayList<>();
        phases.add( new Phase( new Vector2D( 240, 50 ), 1f ) );


        ArrayList< Turret > turrets = new ArrayList<>();
        turrets.add( new Ballistic( TurretTypes.BALLISTIC_SMALL, 0 ) );
        turrets.add( new Ballistic( TurretTypes.BALLISTIC_SMALL, 0 ) );
        turrets.add( new Ballistic( TurretTypes.BALLISTIC_SMALL_DOUBLE, 0 ) );

        level.enemyShip = new EnemyShip( ShipTypes.GUN_DESTROYER, turrets, level.shipHealth, phases );

        setUpPlayer( level );

        level.objects.add( level.enemyShip );
        level.objects.add( level.playerShip );

        level.enemyShip.setTargets( 2, level.playerShip );
    }

    public static void levelFive( Level level )
    {

        level.levelEnded = false;
        level.levelWon = false;
        level.levelType = 5;
        level.shipHealth = 60;

        ArrayList< Phase > phases = new ArrayList<>();
        phases.add( new Phase( new Vector2D( 375, 50 ), .75f ) );
        phases.add( new Phase( new Vector2D( -100, 50 ), .75f ) );

        ArrayList< Turret > turrets = new ArrayList<>();
        turrets.add( new Ballistic( TurretTypes.BALLISTIC_CRUISER, 0 ) );
        turrets.add( new Ballistic( TurretTypes.BALLISTIC_CRUISER, 0 ) );
        turrets.add( new Ballistic( TurretTypes.BALLISTIC_SMALL_DOUBLE, 0 ) );
        turrets.add( new Missile( TurretTypes.LARGE_MISSILE, 0 ) );
        turrets.add( new Ballistic( TurretTypes.BALLISTIC_SMALL_DOUBLE, 0 ) );
        turrets.add( new Ballistic( TurretTypes.BALLISTIC_SMALL_DOUBLE, 0 ) );

        level.enemyShip = new EnemyShip( ShipTypes.CRUISER, turrets, level.shipHealth, phases );

        setUpPlayer( level );

        level.objects.add( level.enemyShip );
        level.objects.add( level.playerShip );

        level.enemyShip.setTargets( 2, level.playerShip );
    }

    public static void levelSix( Level level )
    {

        level.levelEnded = false;
        level.levelWon = false;
        level.levelType = 6;
        level.shipHealth = 40;

        ArrayList< Phase > phases = new ArrayList<>();
        phases.add( new Phase( new Vector2D( 240, 50 ), 1f ) );

        ArrayList< Turret > turrets = new ArrayList<>();
        turrets.add( new Ballistic( TurretTypes.BALLISTIC_LARGE, 0 ) );
        turrets.add( new Missile( TurretTypes.SMALL_MISSILE, 0 ) );
        turrets.add( new Missile( TurretTypes.SMALL_MISSILE, 0 ) );

        level.enemyShip = new EnemyShip( ShipTypes.MISSILE_DESTROYER, turrets, level.shipHealth, phases );

        setUpPlayer( level );

        level.objects.add( level.enemyShip );
        level.objects.add( level.playerShip );

        level.enemyShip.setTargets( 2, level.playerShip );
    }

    public static void levelSeven( Level level )
    {

        level.levelEnded = false;
        level.levelWon = false;
        level.levelType = 7;
        level.shipHealth = 50;

        ArrayList< Phase > phases = new ArrayList<>();
        phases.add( new Phase( new Vector2D( 240, 50 ), 1f ) );

        ArrayList< Turret > turrets = new ArrayList<>();
        turrets.add( new Missile( TurretTypes.MINI_ROCKET, 0 ) );
        turrets.add( new Ballistic( TurretTypes.BALLISTIC_SMALL_DOUBLE, 0 ) );
        turrets.add( new Missile( TurretTypes.SMALL_MISSILE, 0 ) );
        turrets.add( new Missile( TurretTypes.MINI_ROCKET, 0 ) );

        level.enemyShip = new EnemyShip( ShipTypes.HUNTER, turrets, level.shipHealth, phases );

        setUpPlayer( level );

        level.objects.add( level.enemyShip );
        level.objects.add( level.playerShip );

        level.enemyShip.setTargets( 2, level.playerShip );
    }

    public static void levelEight( Level level )
    {

        level.levelEnded = false;
        level.levelWon = false;
        level.levelType = 8;
        level.shipHealth = 40;

        ArrayList< Phase > phases = new ArrayList<>();
        phases.add( new Phase( new Vector2D( 240, 50 ), 1f ) );

        ArrayList< Turret > turrets = new ArrayList<>();
        turrets.add( new Laser( TurretTypes.SMALL_LASER, 0 ) );
        turrets.add( new Missile( TurretTypes.SMALL_MISSILE, 0 ) );
        turrets.add( new Missile( TurretTypes.SMALL_MISSILE, 0 ) );

        level.enemyShip = new EnemyShip( ShipTypes.MISSILE_DESTROYER, turrets, level.shipHealth, phases );

        setUpPlayer( level );

        level.objects.add( level.enemyShip );
        level.objects.add( level.playerShip );

        level.enemyShip.setTargets( 2, level.playerShip );
    }

    public static void levelNine( Level level )
    {

        level.levelEnded = false;
        level.levelWon = false;
        level.levelType = 9;
        level.shipHealth = 60;

        ArrayList< Phase > phases = new ArrayList<>();
        phases.add( new Phase( new Vector2D( 375, 50 ), .75f ) );
        phases.add( new Phase( new Vector2D( -100, 50 ), .75f ) );

        ArrayList< Turret > turrets = new ArrayList<>();
        turrets.add( new Ballistic( TurretTypes.MEDIUM_LASER, 0 ) );
        turrets.add( new Ballistic( TurretTypes.MEDIUM_LASER, 0 ) );
        turrets.add( new Ballistic( TurretTypes.BALLISTIC_SMALL_DOUBLE, 0 ) );
        turrets.add( new Missile( TurretTypes.LARGE_MISSILE, 0 ) );
        turrets.add( new Ballistic( TurretTypes.BALLISTIC_SMALL_DOUBLE, 0 ) );
        turrets.add( new Ballistic( TurretTypes.BALLISTIC_SMALL_DOUBLE, 0 ) );

        level.enemyShip = new EnemyShip( ShipTypes.CRUISER, turrets, level.shipHealth, phases );

        setUpPlayer( level );

        level.objects.add( level.enemyShip );
        level.objects.add( level.playerShip );

        level.enemyShip.setTargets( 2, level.playerShip );
    }

    public static void levelTen( Level level )
    {

        level.levelEnded = false;
        level.levelWon = false;
        level.levelType = 10;
        level.shipHealth = 60;

        ArrayList< Phase > phases = new ArrayList<>();
        phases.add( new Phase( new Vector2D( 250, 50 ), .5f ) );
        phases.add( new Phase( new Vector2D( -240, 50 ), .5f ) );

        ArrayList< Turret > turrets = new ArrayList<>();
        turrets.add( new Ballistic( TurretTypes.BALLISTIC_CRUISER, 0 ) );
        turrets.add( new Laser( TurretTypes.MEDIUM_LASER, 0 ) );
        turrets.add( new Ballistic( TurretTypes.BALLISTIC_LARGE, 0 ) );
        turrets.add( new Ballistic( TurretTypes.BALLISTIC_LARGE, 0 ) );

        turrets.add( new Ballistic( TurretTypes.BALLISTIC_LARGE, 0 ) );
        turrets.add( new Missile( TurretTypes.MINI_ROCKET, 0 ) );
        turrets.add( new Missile( TurretTypes.MINI_ROCKET, 0 ) );
        turrets.add( new Laser( TurretTypes.LARGE_LASER, 0 ) );


        level.enemyShip = new EnemyShip( ShipTypes.BATTLESHIP, turrets, level.shipHealth, phases );

        setUpPlayer( level );

        level.objects.add( level.enemyShip );
        level.objects.add( level.playerShip );

        level.enemyShip.setTargets( 2, level.playerShip );
    }

    public static void levelEleven( Level level )
    {

        level.levelEnded = false;
        level.levelWon = false;
        level.levelType = 11;
        level.shipHealth = 60;

        ArrayList< Phase > phases = new ArrayList<>();
        phases.add( new Phase( new Vector2D( 240, 50 ), 1f ) );

        ArrayList< Turret > turrets = new ArrayList<>();
        turrets.add( new Missile( TurretTypes.MINI_ROCKET, 0 ) );
        turrets.add( new Ballistic( TurretTypes.BALLISTIC_SMALL_DOUBLE, 0 ) );
        turrets.add( new Missile( TurretTypes.SMALL_MISSILE, 0 ) );
        turrets.add( new Missile( TurretTypes.MINI_ROCKET, 0 ) );

        level.enemyShip = new EnemyShip( ShipTypes.HUNTER, turrets, level.shipHealth, phases );

        setUpPlayer( level );

        level.objects.add( level.enemyShip );
        level.objects.add( level.playerShip );

        level.enemyShip.setTargets( 2, level.playerShip );
    }

    public static void levelTwelve( Level level )
    {

        level.levelEnded = false;
        level.levelWon = false;
        level.levelType = 12;
        level.shipHealth = 60;

        ArrayList< Phase > phases = new ArrayList<>();
        phases.add( new Phase( new Vector2D( 240, 50 ), 1f ) );

        ArrayList< Turret > turrets = new ArrayList<>();
        turrets.add( new Laser( TurretTypes.SMALL_LASER, 0 ) );
        turrets.add( new Missile( TurretTypes.SMALL_MISSILE, 0 ) );
        turrets.add( new Laser( TurretTypes.SMALL_LASER, 0 ) );

        level.enemyShip = new EnemyShip( ShipTypes.MISSILE_DESTROYER, turrets, level.shipHealth, phases );

        setUpPlayer( level );

        level.objects.add( level.enemyShip );
        level.objects.add( level.playerShip );

        level.enemyShip.setTargets( 2, level.playerShip );
    }

    public static void levelThirteen( Level level )
    {

        level.levelEnded = false;
        level.levelWon = false;
        level.levelType = 13;
        level.shipHealth = 50;

        ArrayList< Phase > phases = new ArrayList<>();
        phases.add( new Phase( new Vector2D( 375, 50 ), .75f ) );
        phases.add( new Phase( new Vector2D( -100, 50 ), 0f ) );

        ArrayList< Turret > turrets = new ArrayList<>();
        turrets.add( new Laser( TurretTypes.MEDIUM_LASER, 0 ) );
        turrets.add( new Laser( TurretTypes.MEDIUM_LASER, 0 ) );
        turrets.add( new Missile( TurretTypes.MINI_ROCKET, 0 ) );

        turrets.add( new Laser( TurretTypes.MEDIUM_LASER, 0 ) );
        turrets.add( new Missile( TurretTypes.MINI_ROCKET, 0 ) );
        turrets.add( new Missile( TurretTypes.MINI_ROCKET, 0 ) );

        level.enemyShip = new EnemyShip( ShipTypes.CRUISER, turrets, level.shipHealth, phases );

        setUpPlayer( level );

        level.objects.add( level.enemyShip );
        level.objects.add( level.playerShip );

        level.enemyShip.setTargets( 2, level.playerShip );
    }

    public static void levelFourteen( Level level )
    {

        level.levelEnded = false;
        level.levelWon = false;
        level.levelType = 14;
        level.shipHealth = 60;

        ArrayList< Phase > phases = new ArrayList<>();
        phases.add( new Phase( new Vector2D( 250, 50 ), .75f ) );
        phases.add( new Phase( new Vector2D( -240, 50 ), 0f ) );

        ArrayList< Turret > turrets = new ArrayList<>();
        turrets.add( new Laser( TurretTypes.MEDIUM_LASER, 0 ) );
        turrets.add( new Laser( TurretTypes.MEDIUM_LASER, 0 ) );
        turrets.add( new Ballistic( TurretTypes.BALLISTIC_LARGE, 0 ) );
        turrets.add( new Ballistic( TurretTypes.BALLISTIC_LARGE, 0 ) );

        turrets.add( new Ballistic( TurretTypes.BALLISTIC_LARGE, 0 ) );
        turrets.add( new Missile( TurretTypes.SMALL_MISSILE, 0 ) );
        turrets.add( new Missile( TurretTypes.SMALL_MISSILE, 0 ) );
        turrets.add( new Laser( TurretTypes.LARGE_LASER, 0 ) );

        level.enemyShip = new EnemyShip( ShipTypes.BATTLESHIP, turrets, level.shipHealth, phases );

        setUpPlayer( level );

        level.objects.add( level.enemyShip );
        level.objects.add( level.playerShip );

        level.enemyShip.setTargets( 2, level.playerShip );
    }

    public static void levelFifteen( Level level )
    {

        level.levelEnded = false;
        level.levelWon = false;
        level.levelType = 15;
        level.shipHealth = 200;

        ArrayList< Phase > phases = new ArrayList<>();
        phases.add( new Phase( new Vector2D( 900, 50 ), .75f ) );
        phases.add( new Phase( new Vector2D( 540, 50 ), .5f ) );
        phases.add( new Phase( new Vector2D( 40, 50 ), .25f ) );
        phases.add( new Phase( new Vector2D( -430, 50 ), 0f ) );

        ArrayList< Turret > turrets = new ArrayList<>();
        turrets.add( new Laser( TurretTypes.MASSIVE_LASER, 0 ) );
        turrets.add( new Ballistic( TurretTypes.BALLISTIC_SMALL_DOUBLE, 0 ) );
        turrets.add( new Ballistic( TurretTypes.BALLISTIC_SMALL_DOUBLE, 0 ) );

        turrets.add( new Laser( TurretTypes.MASSIVE_LASER, 0 ) );
        turrets.add( new Ballistic( TurretTypes.BALLISTIC_SMALL_DOUBLE, 0 ) );
        turrets.add( new Missile( TurretTypes.MINI_ROCKET, 0 ) );
        turrets.add( new Missile( TurretTypes.MINI_ROCKET, 0 ) );

        turrets.add( new Ballistic( TurretTypes.BALLISTIC_SMALL_DOUBLE, 0 ) );
        turrets.add( new Ballistic( TurretTypes.BALLISTIC_SMALL_DOUBLE, 0 ) );
        turrets.add( new Missile( TurretTypes.SMALL_MISSILE, 0 ) );
        turrets.add( new Missile( TurretTypes.SMALL_MISSILE, 0 ) );
        turrets.add( new Missile( TurretTypes.SMALL_MISSILE, 0 ) );

        turrets.add( new SuperLaser( TurretTypes.SUPER_LASER, 0 ) );
        turrets.add( new Ballistic( TurretTypes.BALLISTIC_SMALL_DOUBLE, 0 ) );
        turrets.add( new Missile( TurretTypes.MINI_ROCKET, 0 ) );
        turrets.add( new Missile( TurretTypes.MINI_ROCKET, 0 ) );

        level.enemyShip = new EnemyShip( ShipTypes.DREADNOUGHT, turrets, level.shipHealth, phases );

        setUpPlayer( level );

        level.objects.add( level.enemyShip );
        level.objects.add( level.playerShip );

        level.enemyShip.setTargets( 2, level.playerShip );
    }

    private static void setUpPlayer( Level level )
    {
        ArrayList< ShieldGenerator > shieldGenerators = new ArrayList<>();
        int temp = 750;
        shieldGenerators.add( new ShieldGenerator( new Vector2D( 82, temp ) ) );
        shieldGenerators.add( new ShieldGenerator( new Vector2D( 240, temp ) ) );
        shieldGenerators.add( new ShieldGenerator( new Vector2D( 340, temp ) ) );
        shieldGenerators.add( new ShieldGenerator( new Vector2D( 427, temp ) ) );

        level.playerHealth = 100;

        level.setPlayerShip( new PlayerShip( new Vector2D( 240, 750 ), shieldGenerators, level.playerHealth ) );
    }

    enum LevelNum
    {
        LEVEL_ONE, LEVEL_TWO, LEVEL_THREE, LEVEL_FOUR, LEVEL_FIVE,
        LEVEL_SIX, LEVEL_SEVEN, LEVEL_EIGHT, LEVEL_NINE, LEVEL_TEN,
        LEVEL_ELEVEN, LEVEL_TWELVE, LEVEL_THIRTEEN, LEVEL_FOURTEEN, LEVEL_FIFTEEN
    }
}
