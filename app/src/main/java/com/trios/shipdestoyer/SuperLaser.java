package com.trios.shipdestoyer;

import android.graphics.Bitmap;

import com.trios.shipdestoyer.lib.DrawUtil;
import com.trios.shipdestoyer.lib.SoundManager;
import com.trios.shipdestoyer.lib.Vector2D;

import java.util.ArrayList;

/**
 * Created by sandworm on 5/16/2017.
 */

public class SuperLaser extends Turret
{
    private ArrayList< Bitmap > glows = new ArrayList<>();
    private ArrayList< Bitmap > rail = new ArrayList<>();
    private Bitmap turretBase;
    private int barrelLength;
    private float soundSpeed;

    /**
     * @param type
     * @param facing
     */
    public SuperLaser( TurretTypes type, int facing )
    {
        super( type, facing );
        turretBase = BitmapLibrary.getBitmap( BitmapLibrary.BitmapFile.SUPER_LASER_TURRET );

        glows.add( BitmapLibrary.getBitmap( BitmapLibrary.BitmapFile.SUPER_LASER_NODES ) );
        glows.add( BitmapLibrary.getBitmap( BitmapLibrary.BitmapFile.SUPER_LASER_VEINS ) );

        rail.add( BitmapLibrary.getBitmap( BitmapLibrary.BitmapFile.SUPER_LASER_FLASH1 ) );
        rail.add( BitmapLibrary.getBitmap( BitmapLibrary.BitmapFile.SUPER_LASER_FLASH2 ) );
        rail.add( BitmapLibrary.getBitmap( BitmapLibrary.BitmapFile.SUPER_LASER_FLASH3 ) );
        rail.add( BitmapLibrary.getBitmap( BitmapLibrary.BitmapFile.SUPER_LASER_FLASH4 ) );
        rail.add( BitmapLibrary.getBitmap( BitmapLibrary.BitmapFile.SUPER_LASER_FLASH5 ) );
        rail.add( BitmapLibrary.getBitmap( BitmapLibrary.BitmapFile.SUPER_LASER_FLASH6 ) );
        rail.add( BitmapLibrary.getBitmap( BitmapLibrary.BitmapFile.SUPER_LASER_FLASH7 ) );

        FIRE_RATE = 200;
        offSet.add( -15 );
        offSet.add( -12 );
        offSet.add( -10 );
        offSet.add( -7 );
        offSet.add( -5 );
        offSet.add( -2 );
        offSet.add( 0 );
        offSet.add( 2 );
        offSet.add( 5 );
        offSet.add( 7 );
        offSet.add( 10 );
        offSet.add( 12 );
        offSet.add( 15 );
        barrelLength = turretBase.getHeight() / 7;
        soundSpeed = 0.f;

        damage = 2;
    }

    public void draw()
    {
        super.draw();

        DrawUtil.graphic( turretBase, pos, ( int ) facing );

        if ( currentFireRate <= FIRE_RATE / 2 )
            if ( currentFireRate <= FIRE_RATE / 4 )
                DrawUtil.graphic( glows.get( 0 ), pos, ( int ) facing, ( int ) ( ( ( double ) currentFireRate / ( ( double ) FIRE_RATE / ( double ) 4 ) ) * 255 ) );
            else
            {
                DrawUtil.graphic( glows.get( 0 ), pos, ( int ) facing );
                DrawUtil.graphic( glows.get( 1 ), pos, ( int ) facing, ( int ) ( ( ( ( double ) currentFireRate - ( ( double ) FIRE_RATE / ( double ) 4 ) ) / ( ( double ) FIRE_RATE / ( double ) 4 ) ) * 255 ) );
            }
        else
        {
            for ( Bitmap bit : glows )
                DrawUtil.graphic( bit, pos, ( int ) facing );

            for ( int i = 1; i <= rail.size(); i++ )
            {
                if ( currentFireRate - ( FIRE_RATE / 2 ) >= ( ( ( FIRE_RATE / 2 ) / ( rail.size() + 1 ) ) * i ) )
                    DrawUtil.graphic( rail.get( i - 1 ), pos, ( int ) facing );
            }
        }

        if ( currentFireRate - ( FIRE_RATE / 2 ) == 0 )
        {
            SoundManager.playSound( 0.5f, SoundManager.LASER_CHARGING, 0, 0, soundSpeed );
        }
    }


    @Override
    public Boolean fireBullet()
    {
        if ( !lockedOn )
            return false;
        currentFireRate = 0;
        for ( int shot :
                offSet )
        {
            double disX = ( Math.sin( Math.toRadians( facing ) ) * shot );
            double disY = ( Math.cos( Math.toRadians( facing ) ) * shot );

            double barX = ( Math.sin( Math.toRadians( facing ) ) * barrelLength );
            double barY = ( Math.cos( Math.toRadians( facing ) ) * barrelLength );
            projectiles.add( new Projectile( this.getPos().add( new Vector2D( disY, disX ) ).add( new Vector2D( -barX, barY ) ),
                                             target, projectileSpeed, this.getType(), damage ) );
        }

        return true;
    }
}
