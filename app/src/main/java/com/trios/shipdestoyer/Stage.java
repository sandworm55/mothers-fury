package com.trios.shipdestoyer;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.warting.bubbles.ConversationActivity;

import java.util.ArrayList;

/**
 * Created by sandw on 5/2/2017.
 */

public class Stage extends Activity implements View.OnClickListener
{
    private ArrayList< Button > missions = new ArrayList<>();
    private ArrayList< Button > convos = new ArrayList<>();
    private SharedPreferences levelPrefs;

    @Override
    protected void onCreate( @Nullable Bundle savedInstanceState )
    {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_stage );

        missions.add( ( Button ) findViewById( R.id.stageOne ) );
        missions.add( ( Button ) findViewById( R.id.stageTwo ) );
        missions.add( ( Button ) findViewById( R.id.stageThree ) );
        missions.add( ( Button ) findViewById( R.id.stageFour ) );
        missions.add( ( Button ) findViewById( R.id.stageFive ) );
        missions.add( ( Button ) findViewById( R.id.stageSix ) );
        missions.add( ( Button ) findViewById( R.id.stageSeven ) );
        missions.add( ( Button ) findViewById( R.id.stageEight ) );
        missions.add( ( Button ) findViewById( R.id.stageNine ) );
        missions.add( ( Button ) findViewById( R.id.stageTen ) );
        missions.add( ( Button ) findViewById( R.id.stageEleven ) );
        missions.add( ( Button ) findViewById( R.id.stageTwelve ) );
        missions.add( ( Button ) findViewById( R.id.stageThirteen ) );
        missions.add( ( Button ) findViewById( R.id.stageFourteen ) );
        missions.add( ( Button ) findViewById( R.id.stageFifteen ) );

        convos.add( ( Button ) findViewById( R.id.convoOne ) );
        convos.add( ( Button ) findViewById( R.id.convoTwo ) );
        convos.add( ( Button ) findViewById( R.id.convoThree ) );
        convos.add( ( Button ) findViewById( R.id.convoFour ) );
        convos.add( ( Button ) findViewById( R.id.convoFive ) );
        convos.add( ( Button ) findViewById( R.id.convoSix ) );
        convos.add( ( Button ) findViewById( R.id.convoSeven ) );
        convos.add( ( Button ) findViewById( R.id.convoEight ) );
        convos.add( ( Button ) findViewById( R.id.convoNine ) );
        convos.add( ( Button ) findViewById( R.id.convoTen ) );
        convos.add( ( Button ) findViewById( R.id.convoEleven ) );
        convos.add( ( Button ) findViewById( R.id.convoTwelve ) );
        convos.add( ( Button ) findViewById( R.id.convoThirteen ) );
        convos.add( ( Button ) findViewById( R.id.convoFourteen ) );
        convos.add( ( Button ) findViewById( R.id.convoFifteen ) );


        levelPrefs = getSharedPreferences( com.trios.shipdestoyer.View.COMPLETED_LEVEL, MODE_PRIVATE );
        com.trios.shipdestoyer.View.completedLevel = levelPrefs.getInt( com.trios.shipdestoyer.View.COMPLETED_LEVEL, 0 );

        missions.get( 0 ).setOnClickListener( this );
        convos.get( 0 ).setOnClickListener( this );
        for ( int i = 1; i < missions.size(); i++ )
        {
            missions.get( i ).setOnClickListener( this );
            convos.get( i ).setOnClickListener( this );

            if ( com.trios.shipdestoyer.View.completedLevel < i )
            {
                missions.get( i ).setEnabled( false );
                missions.get( i ).setAlpha( .25f );
                convos.get( i ).setEnabled( false );
                convos.get( i ).setEnabled( false );
                convos.get( i ).setAlpha( .25f );
            }
        }
    }

    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    @Override
    public void onClick( View v )
    {
        Intent i;
        switch ( v.getId() )
        {
            case R.id.stageOne:
                i = new Intent( this, GameActivity.class );
                LevelManager.currentLevel = LevelManager.LevelNum.LEVEL_ONE;
                break;
            case R.id.convoOne:
                i = new Intent( this, ConversationActivity.class );
                ConversationActivity.setConvoOnePre();
                break;

            case R.id.stageTwo:
                i = new Intent( this, GameActivity.class );
                LevelManager.currentLevel = LevelManager.LevelNum.LEVEL_TWO;
                break;
            case R.id.convoTwo:
                i = new Intent( this, ConversationActivity.class );
                ConversationActivity.setConvoTwoPre();
                break;

            case R.id.stageThree:
                i = new Intent( this, GameActivity.class );
                LevelManager.currentLevel = LevelManager.LevelNum.LEVEL_THREE;
                break;
            case R.id.convoThree:
                i = new Intent( this, ConversationActivity.class );
                ConversationActivity.setConvoThreePre();
                break;

            case R.id.stageFour:
                i = new Intent( this, GameActivity.class );
                LevelManager.currentLevel = LevelManager.LevelNum.LEVEL_FOUR;
                break;
            case R.id.convoFour:
                i = new Intent( this, ConversationActivity.class );
                ConversationActivity.setConvoFourPre();
                break;

            case R.id.stageFive:
                i = new Intent( this, GameActivity.class );
                LevelManager.currentLevel = LevelManager.LevelNum.LEVEL_FIVE;
                break;
            case R.id.convoFive:
                i = new Intent( this, ConversationActivity.class );
                ConversationActivity.setConvoFivePre();
                break;

            case R.id.stageSix:
                i = new Intent( this, GameActivity.class );
                LevelManager.currentLevel = LevelManager.LevelNum.LEVEL_SIX;
                break;
            case R.id.convoSix:
                i = new Intent( this, ConversationActivity.class );
                ConversationActivity.setConvoSixPre();
                break;

            case R.id.stageSeven:
                i = new Intent( this, GameActivity.class );
                LevelManager.currentLevel = LevelManager.LevelNum.LEVEL_SEVEN;
                break;
            case R.id.convoSeven:
                i = new Intent( this, ConversationActivity.class );
                ConversationActivity.setConvoSevenPre();
                break;

            case R.id.stageEight:
                i = new Intent( this, GameActivity.class );
                LevelManager.currentLevel = LevelManager.LevelNum.LEVEL_EIGHT;
                break;
            case R.id.convoEight:
                i = new Intent( this, ConversationActivity.class );
                ConversationActivity.setConvoEightPre();
                break;

            case R.id.stageNine:
                i = new Intent( this, GameActivity.class );
                LevelManager.currentLevel = LevelManager.LevelNum.LEVEL_NINE;
                break;
            case R.id.convoNine:
                i = new Intent( this, ConversationActivity.class );
                ConversationActivity.setConvoNinePre();
                break;

            case R.id.stageTen:
                i = new Intent( this, GameActivity.class );
                LevelManager.currentLevel = LevelManager.LevelNum.LEVEL_TEN;
                break;
            case R.id.convoTen:
                i = new Intent( this, ConversationActivity.class );
                ConversationActivity.setConvoTenPre();
                break;

            case R.id.stageEleven:
                i = new Intent( this, GameActivity.class );
                LevelManager.currentLevel = LevelManager.LevelNum.LEVEL_ELEVEN;
                break;
            case R.id.convoEleven:
                i = new Intent( this, ConversationActivity.class );
                ConversationActivity.setConvoElevenPre();
                break;

            case R.id.stageTwelve:
                i = new Intent( this, GameActivity.class );
                LevelManager.currentLevel = LevelManager.LevelNum.LEVEL_TWELVE;
                break;
            case R.id.convoTwelve:
                i = new Intent( this, ConversationActivity.class );
                ConversationActivity.setConvoTwelvePre();
                break;

            case R.id.stageThirteen:
                i = new Intent( this, GameActivity.class );
                LevelManager.currentLevel = LevelManager.LevelNum.LEVEL_THIRTEEN;
                break;
            case R.id.convoThirteen:
                i = new Intent( this, ConversationActivity.class );
                ConversationActivity.setConvoThirteenPre();
                break;

            case R.id.stageFourteen:
                i = new Intent( this, GameActivity.class );
                LevelManager.currentLevel = LevelManager.LevelNum.LEVEL_FOURTEEN;
                break;
            case R.id.convoFourteen:
                i = new Intent( this, ConversationActivity.class );
                ConversationActivity.setConvoFourteenPre();
                break;

            case R.id.stageFifteen:
                i = new Intent( this, GameActivity.class );
                LevelManager.currentLevel = LevelManager.LevelNum.LEVEL_FIFTEEN;
                break;
            case R.id.convoFifteen:
                i = new Intent( this, ConversationActivity.class );
                ConversationActivity.setConvoFifteenPre();
                break;
            default:
                i = null;
        }
        Log.d( "reload", "staring from intent " + i );
        startActivity( i );
    }
}
