package com.trios.shipdestoyer;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.Switch;

/**
 * Created by sandw on 5/1/2017.
 */

public class Options extends Activity implements View.OnClickListener, SeekBar.OnSeekBarChangeListener
{
    public static final String OPTION = "options";
    public static final String
            OPTIONS_SFX_BOOL_STRING = "options_sfx_bool",
            OPTIONS_MUSIC_BOOL_STRING = "options_music_bool",
            OPTIONS_SFX_INT_STRING = "options_sfx_int",
            OPTIONS_MUSIC_INT_STRING = "options_music_int";
    public static int sfxVolume, musicVolume;
    public static boolean sfxToggle, musicToggle;
    public static SharedPreferences optionsPrefs;
    public static SharedPreferences.Editor optionsEditor;
    private static SharedPreferences levelPrefs;
    private static SharedPreferences.Editor editor;

    public static void init( Context context )
    {

        optionsPrefs = context.getSharedPreferences( OPTION, MODE_PRIVATE );
        optionsEditor = optionsPrefs.edit();

        sfxToggle = optionsPrefs.getBoolean( OPTIONS_SFX_BOOL_STRING, true );

        sfxVolume = optionsPrefs.getInt( OPTIONS_SFX_INT_STRING, 5 );

        musicToggle = optionsPrefs.getBoolean( OPTIONS_MUSIC_BOOL_STRING, true );

        musicVolume = optionsPrefs.getInt( OPTIONS_MUSIC_INT_STRING, 5 );
    }

    public static float getSfxVolume()
    {
        if ( sfxToggle )
            return sfxVolume;
        return 0;
    }

    @Override
    protected void onCreate( Bundle savedInstance )
    {
        super.onCreate( savedInstance );
        setContentView( R.layout.activity_options );

        optionsPrefs = getSharedPreferences( OPTION, MODE_PRIVATE );
        optionsEditor = optionsPrefs.edit();

        final Switch toggleSfx = ( Switch ) findViewById( R.id.toggleSfx );
        toggleSfx.setOnClickListener( this );
        toggleSfx.setChecked( optionsPrefs.getBoolean( OPTIONS_SFX_BOOL_STRING, true ) );

        final SeekBar seekSfx = ( SeekBar ) findViewById( R.id.seekSfx );
        seekSfx.setOnSeekBarChangeListener( this );
        seekSfx.setProgress( optionsPrefs.getInt( OPTIONS_SFX_INT_STRING, 5 ) );

        final Switch toggleMusic = ( Switch ) findViewById( R.id.toggleMusic );
        toggleMusic.setOnClickListener( this );
        toggleMusic.setChecked( optionsPrefs.getBoolean( OPTIONS_MUSIC_BOOL_STRING, true ) );

        final SeekBar seekMusic = ( SeekBar ) findViewById( R.id.seekMusic );
        seekMusic.setOnSeekBarChangeListener( this );
        seekMusic.setProgress( optionsPrefs.getInt( OPTIONS_MUSIC_INT_STRING, 5 ) );

        final Button btnReset = ( Button ) findViewById( R.id.btnReset );
        btnReset.setOnClickListener( this );

        final Button btnCheater = ( Button ) findViewById( R.id.btnCheater );
        btnCheater.setOnClickListener( this );
    }

    @Override
    public void onClick( View v )
    {
        switch ( v.getId() )
        {
            case R.id.toggleSfx:
                sfxToggle = ( ( Switch ) findViewById( R.id.toggleSfx ) ).isChecked();
                optionsEditor.putBoolean( OPTIONS_SFX_BOOL_STRING, sfxToggle );
                optionsEditor.commit();
                break;
            case R.id.toggleMusic:
                musicToggle = ( ( Switch ) findViewById( R.id.toggleMusic ) ).isChecked();
                optionsEditor.putBoolean( OPTIONS_MUSIC_BOOL_STRING, musicToggle );
                optionsEditor.commit();
                break;

            case R.id.btnReset:
                levelPrefs = getSharedPreferences( com.trios.shipdestoyer.View.COMPLETED_LEVEL, MODE_PRIVATE );
                editor = levelPrefs.edit();
                com.trios.shipdestoyer.View.completedLevel = 0;
                editor.putInt( com.trios.shipdestoyer.View.COMPLETED_LEVEL, 0 );
                editor.commit();
                break;

            case R.id.btnCheater:
                levelPrefs = getSharedPreferences( com.trios.shipdestoyer.View.COMPLETED_LEVEL, MODE_PRIVATE );
                editor = levelPrefs.edit();
                com.trios.shipdestoyer.View.completedLevel = 15;
                editor.putInt( com.trios.shipdestoyer.View.COMPLETED_LEVEL, 15 );
                editor.commit();
                break;
        }
    }

    @Override
    public void onProgressChanged( SeekBar seekBar, int progress, boolean fromUser )
    {

        if ( seekBar == findViewById( R.id.seekSfx ) )
        {
            sfxVolume = ( seekBar ).getProgress();
            optionsEditor.putInt( OPTIONS_SFX_INT_STRING, sfxVolume );
            optionsEditor.commit();
        }

        if ( seekBar == findViewById( R.id.seekMusic ) )
        {
            musicVolume = ( seekBar ).getProgress();
            optionsEditor.putInt( OPTIONS_MUSIC_INT_STRING, musicVolume );
            optionsEditor.commit();
        }
    }

    @Override
    public void onStartTrackingTouch( SeekBar seekBar ) { }

    @Override
    public void onStopTrackingTouch( SeekBar seekBar ) { }
}
