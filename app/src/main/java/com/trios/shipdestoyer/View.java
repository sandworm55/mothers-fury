package com.trios.shipdestoyer;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import com.trios.shipdestoyer.lib.DrawUtil;
import com.trios.shipdestoyer.lib.MathFunc;
import com.trios.shipdestoyer.lib.SoundManager;
import com.trios.shipdestoyer.lib.Vector2D;
import com.warting.bubbles.ConversationActivity;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by Stewart on 2017-04-18.
 */

public class View extends SurfaceView implements Runnable
{
    public static final String COMPLETED_LEVEL = "completedLevel";
    public static SharedPreferences.Editor editor;
    public static int completedLevel;
    public static Context context;
    volatile boolean playing;
    Thread gameThread;
    private Canvas canvas;
    private Paint paint;
    private SurfaceHolder holder;
    private int screenX, screenY;
    private ArrayList< GameObject > objects = new ArrayList<>();
    private ArrayList< BGElement > bgElements = new ArrayList<>();
    private SharedPreferences levelPrefs;
    private boolean gameEnded;
    private Level level;

    public View( Context context, int x, int y )
    {
        super( context );
        this.context = context;
        SoundManager.init( this.context );
        Options.init( this.context );
        screenX = x;
        screenY = y;

        holder = getHolder();
        paint = new Paint();
        BitmapLibrary.clearCanvas();

        levelPrefs = context.getSharedPreferences( COMPLETED_LEVEL, context.MODE_PRIVATE );
        editor = levelPrefs.edit();

        completedLevel = levelPrefs.getInt( COMPLETED_LEVEL, 0 );

        startGame();
    }

    private void startGame()
    {
        BitmapLibrary.init( context );
        Random random = new Random();
        int temp;
        //draw some stars
        temp = random.nextInt( 15 ) + 15;
        for ( int i = 0; i < temp; i++ )
            bgElements.add( new BGElement( BGElement.Elements.STAR ) );

        //draw some nebula
        temp = random.nextInt( 3 );
        for ( int i = 0; i < temp; i++ )
            bgElements.add( new BGElement( BGElement.Elements.NEBULA ) );

        //draw some more stars
        temp = random.nextInt( 7 ) + 7;
        for ( int i = 0; i < temp; i++ )
            bgElements.add( new BGElement( BGElement.Elements.STAR ) );

        //draw planet... maybe
        temp = random.nextInt( 2 );
        for ( int i = 0; i < temp; i++ )
            bgElements.add( new BGElement( BGElement.Elements.PLANET ) );

        level = new Level();
        this.objects = level.getObjects();
    }

    @Override
    public void run()
    {
        while ( playing )
        {
            update();
            draw();
            control();
        }
    }

    public void update()
    {
        level.update();
        for ( GameObject obj : objects )
            obj.update();
        //        level.enemyShip.setPos( level.enemyShip.getPos( ).add( new Vector2D( -1, 0 ) ) );
        if ( level.isLevelEnded() )
        {
            Intent intent = new Intent( context, EndGameFail.class );
            context.startActivity( intent );
            Activity activity = ( Activity ) context;

            level.reset();

            activity.finish();
            return;
        }
        else if ( level.isLevelWon() )
        {
            Intent intent = new Intent( context, ConversationActivity.class );
            context.startActivity( intent );
            Activity activity = ( Activity ) context;

            level.reset();

            activity.finish();
        }

    }

    public void draw()
    {

        if ( holder.getSurface().isValid() )
        {
            canvas = holder.lockCanvas();

            BitmapLibrary.setCanvas( canvas );

            canvas.drawColor( Color.argb( 255, 0, 0, 40 ) );

            for ( BGElement ele : bgElements )
                ele.draw();

            paint.setColor( Color.argb( 255, 255, 255, 255 ) );

            screenX = canvas.getWidth();
            screenY = canvas.getHeight();

            for ( Turret turret : level.getEnemyShip().getTurrets() )
            {
                if ( turret.getPos() != null && turret.getPos().getX() >= 0 && turret.getPos().getX() <= 480 )
                {
                    double xDist, yDist;
                    if ( turret.lastTarget != null )
                    {
                        xDist = turret.target.getPos().getX() - turret.pos.getX();
                        yDist = turret.target.getPos().getY() - turret.pos.getY();
                        double targetAngle = Math.atan( xDist / -yDist ) * ( 180 / Math.PI );

                        xDist = turret.lastTarget.getX() - turret.pos.getX();
                        yDist = turret.lastTarget.getY() - turret.pos.getY();
                        double lastTargetAngle = Math.atan( xDist / -yDist ) * ( 180 / Math.PI );

                        double percent = ( turret.facing - lastTargetAngle ) / ( targetAngle - lastTargetAngle );
                        if ( percent == 0 )
                            percent = 1;
                        if ( ( targetAngle - lastTargetAngle ) == 0 )
                            percent = 1;

                        DrawUtil.drawTrackLine( turret.pos, turret.target.getPos(), ( int ) ( 50 * ( percent ) ) );
                    }
                }
            }

            if ( objects != null )
                for ( GameObject obj : objects )
                    obj.draw();
            for ( Turret tur : level.getEnemyShip().getTurrets() )
                for ( HitEffect eff : tur.getHitEffects() )
                    eff.draw();

            DrawUtil.drawText( "INTEGRITY: " + ( int ) ( ( ( float ) Level.shipHealth / EnemyShip.getTotalHealth() ) * 100 ) + "%", new Vector2D( 10, 200 ), Color.rgb( 255, 255, 255 ) );
            DrawUtil.drawText( "INTEGRITY: " + ( int ) ( ( ( float ) Level.playerHealth / PlayerShip.getTotalHealth() ) * 100 ) + "%", new Vector2D( 10, 650 ), Color.rgb( 255, 255, 255 ) );

            holder.unlockCanvasAndPost( canvas );
        }
    }

    public void control()
    {
        try
        {
            gameThread.sleep( 17 );
        } catch ( InterruptedException e )
        {
        }
    }

    public void pause()
    {
        playing = false;
        try
        {
            gameThread.join();
        } catch ( InterruptedException e )
        {
        }
    }

    public void resume()
    {
        playing = true;
        gameThread = new Thread( this );
        gameThread.start();
    }

    @Override
    public boolean onTouchEvent( MotionEvent motionEvent )
    {
        handleInput( motionEvent );
        return true;
    }

    private void handleInput( MotionEvent motionEvent )
    {
        int i = motionEvent.getActionIndex();
        Vector2D pos = new Vector2D( MathFunc.convertXFrom( ( int ) motionEvent.getX( i ), screenX ), MathFunc.convertYFrom( ( int ) motionEvent.getY( i ), screenY ) );

        switch ( motionEvent.getAction() & MotionEvent.ACTION_MASK )
        {
            case MotionEvent.ACTION_POINTER_DOWN:
            case MotionEvent.ACTION_DOWN:
                if ( gameEnded )
                    startGame();
                else
                    for ( ShieldGenerator gen : level.getPlayerShip().getShieldGenerators() )
                        gen.didClick( pos, i );
                break;

            case MotionEvent.ACTION_POINTER_UP:
            case MotionEvent.ACTION_UP:
                for ( ShieldGenerator gen : level.getPlayerShip().getShieldGenerators() )
                    gen.didRelease( i );
                break;
        }
    }
}
