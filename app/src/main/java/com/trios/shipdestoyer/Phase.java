package com.trios.shipdestoyer;

import com.trios.shipdestoyer.lib.Vector2D;


/**
 * Created by seand on 5/18/2017.
 */

public class Phase
{

    private Vector2D pos;
    private float healthCheck;

    public Phase( Vector2D pos, float healthCheck )
    {
        this.pos = pos;
        this.healthCheck = healthCheck;
    }

    public float getHealthCheck()
    {
        return healthCheck;
    }

    public Vector2D getPos()
    {
        return pos;
    }
}
