package com.trios.shipdestoyer;

import android.graphics.Bitmap;

import com.trios.shipdestoyer.lib.DrawUtil;

/**
 * Created by sandworm on 5/12/2017.
 */

public class Missile extends Turret
{
    Bitmap bitmap;

    /**
     * @param type
     * @param facing
     */
    public Missile( TurretTypes type, int facing )
    {
        super( type, facing );
        switch ( type )
        {
            case MINI_ROCKET:
                bitmap = BitmapLibrary.getBitmap( BitmapLibrary.BitmapFile.MINI_ROCKET_TURRET );
                rotationSpeed = .5f;
                projectileSpeed = 1f;
                offSet.add( 0 );
                FIRE_RATE = 150;
                damage = 5;
                break;
            case SMALL_MISSILE:
                bitmap = BitmapLibrary.getBitmap( BitmapLibrary.BitmapFile.SMALL_MISSILE_TURRET );
                rotationSpeed = .5f;
                projectileSpeed = 1f;
                offSet.add( 0 );
                FIRE_RATE = 300;
                damage = 10;
                break;
            case LARGE_MISSILE:
                bitmap = BitmapLibrary.getBitmap( BitmapLibrary.BitmapFile.LARGE_MISSILE_TURRET );
                rotationSpeed = .1f;
                projectileSpeed = .5f;
                offSet.add( 0 );
                FIRE_RATE = 500;
                damage = 15;
                break;
            default:
                bitmap = BitmapLibrary.getBitmap( BitmapLibrary.BitmapFile.SMALL_MISSILE_TURRET );
                damage = 100;
        }
    }

    public void draw()
    {
        super.draw();
        DrawUtil.graphic( bitmap, pos, ( int ) facing );

    }
}
