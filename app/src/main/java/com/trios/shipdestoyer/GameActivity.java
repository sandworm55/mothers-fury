package com.trios.shipdestoyer;

import android.app.Activity;
import android.graphics.Point;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;

/**
 * Created by Stewart on 2017-04-18.
 */

public class GameActivity extends Activity
{

    private View gameView;

    @Override
    protected void onCreate( Bundle savedInstance )
    {
        super.onCreate( savedInstance );

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize( size );


        gameView = new View( this, size.x, size.y );
        setContentView( gameView );
        Log.d( "reload", "making activity " + this );
    }


    @Override
    protected void onResume()
    {
        super.onResume();
        gameView.resume();
    }

    @Override
    protected void onPause()
    {
        super.onPause();
        gameView.pause();
    }

    @Override
    public boolean onKeyDown( int keyCode, KeyEvent event )
    {
        if ( keyCode == KeyEvent.KEYCODE_BACK )
        {
            finish();
            return true;
        }
        return false;
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        BitmapLibrary.destroy();
    }
}


