package com.trios.shipdestoyer;

import android.graphics.Bitmap;

import com.trios.shipdestoyer.lib.DrawUtil;
import com.trios.shipdestoyer.lib.Vector2D;

import java.util.Random;

/**
 * Created by sandworm on 5/17/2017.
 */

public class BGElement
{
    private Bitmap bitmap;
    private Vector2D pos;
    private Elements element;
    private int rot = 0;
    private int opacity = 125;
    private float scale = 0.5f;

    public BGElement( Elements element )
    {
        this.element = element;
        Random random = new Random();
        switch ( element )
        {
            case STAR:
                this.bitmap = BitmapLibrary.getStar();
                this.pos = new Vector2D( random.nextInt( 480 ), random.nextInt( 800 ) );
                this.rot = random.nextInt( 360 ) - 180;
                break;
            case NEBULA:
                this.bitmap = BitmapLibrary.getNebula();
                this.pos = new Vector2D( random.nextInt( 480 ), random.nextInt( 800 ) );
                this.rot = random.nextInt( 360 ) - 180;
                this.opacity = random.nextInt( 30 ) + 100;
                this.scale = 1;
                break;
            case PLANET:
                this.bitmap = BitmapLibrary.getPlanet();
                this.pos = new Vector2D( random.nextInt( 480 ), random.nextInt( 400 ) + 400 );
                this.scale = 1;
                this.opacity = 255;
                break;
        }
    }

    public void draw()
    {
        DrawUtil.graphic( bitmap, pos, new Vector2D( bitmap.getWidth() / 2, bitmap.getHeight() / 2 ), scale, rot, opacity );
    }

    public enum Elements
    {
        STAR, NEBULA, PLANET
    }
}
