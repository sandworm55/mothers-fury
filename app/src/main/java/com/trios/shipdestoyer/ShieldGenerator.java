package com.trios.shipdestoyer;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.RequiresApi;

import com.trios.shipdestoyer.lib.DrawUtil;
import com.trios.shipdestoyer.lib.SoundManager;
import com.trios.shipdestoyer.lib.Vector2D;

/**
 * Created by sandworm on 5/8/2017.
 */

public class ShieldGenerator implements GameObject
{
    private static final float SHIELD_ACTIVE_DELAY = .5f;//not final value, change value or type as needed
    private static final int ANGLE_SWEEP = 140;
    private static int MAX_ENERGY = 100;
    private Vector2D pos;
    private int energy = 100;
    private boolean active = false;
    private boolean timerActive = false;
    private int radius;
    private Bitmap bitmapGen;
    private Bitmap bitmapShield;
    private int timer = 0;
    private int touchId = -1;

    private int streamID;
    private boolean playingSound;


    public ShieldGenerator( Vector2D pos )
    {
        this.pos = pos;
        this.bitmapGen = BitmapLibrary.getBitmap( BitmapLibrary.BitmapFile.SHIELD_GENERATOR );
        this.bitmapShield = BitmapLibrary.getBitmap( BitmapLibrary.BitmapFile.SHIELD );
        this.radius = ( int ) ( ( double ) 69 / 2 * SCALE );
    }

    public static double getSCALE()
    {
        return SCALE;
    }

    @RequiresApi( api = Build.VERSION_CODES.LOLLIPOP )
    @Override
    public void draw()
    {
        DrawUtil.drawArc( pos, radius, Color.CYAN,
                          200, ( int ) ( ( ( float ) energy / ( float ) MAX_ENERGY ) * ANGLE_SWEEP ), true );

        DrawUtil.graphic( bitmapGen, pos, SCALE, 0 );

        if ( energy == 0 )
        {
            deactivate();
            timer = 0;
            timerActive = false;
        }

        if ( active || timerActive )
        {
            timer += 5;

            if ( timer >= 40 )
            {
                DrawUtil.graphic( bitmapShield, pos.add( new Vector2D( 0, -40 - 40 ) ), SCALE, 0 );
            }
            else
            {
                DrawUtil.graphic( bitmapShield, pos.add( new Vector2D( 0, -40 - timer ) ), SCALE, 0 );
            }

            if ( timer < 100 && timer > 0 )
            {
                timerActive = true;
            }
            else if ( timer > 100 && !active )
            {
                timer = 0;
                timerActive = false;
            }
        }
    }

    @Override
    public void update()
    {
        if ( active || timerActive )
        {
            if ( energy > 0 )
                energy--;
        }
        else
        {
            if ( energy < MAX_ENERGY )
                energy++;
        }

        soundLogic();

        //TODO: increment or decrement the activation % basied on the delay
    }

    private void soundLogic()
    {
        if ( active || timerActive )
        {
            if ( !playingSound )
            {
                streamID = SoundManager.playSound( SoundManager.SHIELD_HUM, 0, -1, 1 );
                playingSound = true;
            }
        }
        else if ( playingSound )
        {
            SoundManager.stopSound( streamID );
            playingSound = false;
        }
    }

    public boolean isActive()
    {
        if ( active || timerActive )
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public void activate()
    {
        active = true;
    }

    public void deactivate()
    {
        active = false;
    }

    public Vector2D getPos()
    {
        return pos;
    }

    public void didClick( Vector2D pos, int touchId )
    {
        if ( new Vector2D( pos.getX(), pos.getY() ).subtract( this.pos ).getMagnitude() <= this.radius )
        {
            activate();
            this.touchId = touchId;
        }
    }

    public void didRelease( int touchId )
    {
        if ( touchId == this.touchId )
        {
            this.touchId = -1;
            deactivate();
        }

    }

    public int getRadius()
    {
        return radius;
    }
}


