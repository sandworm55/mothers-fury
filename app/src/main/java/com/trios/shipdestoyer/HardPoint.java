package com.trios.shipdestoyer;

import com.trios.shipdestoyer.lib.Vector2D;

/**
 * Created by sandworm on 5/12/2017.
 */

public final class HardPoint
{
    public static final Vector2D[] GUN_DESTROYER =
            {
                    // 1 phase
                    new Vector2D( -0.2, 0 ), //small destroyer turret, autocannon, small double turret.
                    new Vector2D( -0.07, 0 ), //small destroyer turret, autocannon, small double turret.
                    new Vector2D( 0.2, 0 ) //small destroyer turret, autocannon, small double turret.
            };

    public static final Vector2D[] CORVETTE =
            {
                    // 1 phase
                    new Vector2D( -0.014, -0.019 ), //small vertical missile
                    new Vector2D( -0.04, 0.023 ), //small destroyer turret, autocannon
                    new Vector2D( 0.07, 0.023 ) //small destroyer turret, autocannon
            };

    public static final Vector2D[] HUNTER =
            {
                    // 1 phase
                    new Vector2D( -0.23, 0 ), //mini rocket launcher
                    new Vector2D( -0.06, 0 ), //autocannon
                    new Vector2D( 0.21, 0 ), //small missile launcher, mini rocket launcher
                    new Vector2D( 0.30, 0 ) //mini rocket launcher
            };

    public static final Vector2D[] BATTLESHIP =
            {
                    //phase 1: 250, 50
                    new Vector2D( -0.24, 0 ), //cruiser/battleship turret, medium laser turret
                    new Vector2D( 0, 0 ), //cruiser/battleship turret, medium laser turret
                    new Vector2D( 0.21, 0.12 ), //large destroyer turret, small missile launcher
                    new Vector2D( 0.4, 0.12 ), //large destroyer turret, small missile launcher

                    //phase 2: -240, 50
                    new Vector2D( 0.58, 0.12 ), //large destroyer turret, small missile launcher
                    new Vector2D( 0.7, 0.12 ), //small destroyer turret, mini rocket launcher
                    new Vector2D( 0.77, 0.12 ), //small destroyer turret, mini rocket launcher
                    new Vector2D( 1.005, 0 ) //large laser turret
            };

    public static final Vector2D[] PATROL =
            {
                    // 1 phase
                    new Vector2D( 0.01, 0 ) //small destroyer turret, patrol hybrid turret
            };

    public static final Vector2D[] DREADNOUGHT =

            {
                    //phase 1: 900, 50
                    new Vector2D( -1.305, 0 ), //dreadnought turret 1 or 2, dreadnought long range cannon, massive laser turret
                    new Vector2D( -1.2, 0.19 ), //autocannon
                    new Vector2D( -1.1, 0.19 ), //autocannon

                    //phase 2: 540, 50
                    new Vector2D( -0.405, 0 ), //dreadnought turret 1 or 2, dreadnought long range cannon, massive laser turret
                    new Vector2D( -0.64, 0.19 ), //autocannon
                    new Vector2D( -0.55, 0.19 ), //autocannon, mini rocket launcher
                    new Vector2D( -0.22, 0.195 ), //autocannon, mini rocket launcher

                    //phase 3: 40, 50
                    new Vector2D( 0.195, 0.115 ), //autocannon, small double turret, mini rocket launcher
                    new Vector2D( 0.35, 0.115 ), //autocannon, small double turret, mini rocket launcher
                    new Vector2D( 0.5, 0.115 ), //autocannon, small double turret, mini rocket launcher
                    new Vector2D( 0.63, 0.115 ), //autocannon, small double turret, mini rocket launcher
                    new Vector2D( 0.805, 0.115 ), //autocannon, small double turret, mini rocket launcher

                    //phase 4: -430, 50
                    new Vector2D( 1.3, 0 ), //super lazor
                    new Vector2D( 0.99, 0.195 ), //autocannon, mini rocket launcher
                    new Vector2D( 1.69, 0.25 ), //autocannon, mini rocket launcher
                    new Vector2D( 1.75, 0.25 ) //autocannon, mini rocket launcher
            };

    public static final Vector2D[] MISSILE_DESTROYER =
            {
                    // 1 phase
                    new Vector2D( -0.16, 0 ), //large destroyer turret, small laser turret
                    new Vector2D( 0.17, 0 ), //small missile launcher
                    new Vector2D( 0.32, 0 ) //small missile launcher
            };

    public static final Vector2D[] CRUISER =
            {
                    //phase 1: 375, 50
                    new Vector2D( -0.25, 0 ), //cruiser/battleship turret, medium laser turret, large missile launcher
                    new Vector2D( 0.1, 0 ), //cruiser/battleship turret, medium laser turret, large missile launcher
                    new Vector2D( -0.02, 0.08 ), //small double turret, small laser turret

                    //phase 2: -100, 50
                    new Vector2D( 0.90, 0 ), //cruiser/battleship turret, medium laser turret, large missile launcher
                    new Vector2D( 0.52, .08 ), //small double turret, small laser turret
                    new Vector2D( 0.62, .08 ) //small double turret, small laser turret
            };

    public static Vector2D[] getHardPoint( ShipTypes type )
    {
        Vector2D[] temp;
        switch ( type )
        {
            case GUN_DESTROYER:
                temp = GUN_DESTROYER;
                break;
            case CORVETTE:
                temp = CORVETTE;
                break;
            case HUNTER:
                temp = HUNTER;
                break;
            case BATTLESHIP:
                temp = BATTLESHIP;
                break;
            case PATROL:
                temp = PATROL;
                break;
            case DREADNOUGHT:
                temp = DREADNOUGHT;
                break;
            case MISSILE_DESTROYER:
                temp = MISSILE_DESTROYER;
                break;
            case CRUISER:
                temp = CRUISER;
                break;
            default:
                temp = null;

        }
        return temp;
    }

}
