package com.trios.shipdestoyer;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

/**
 * Created by seand on 5/2/2017.
 */

public class MainMenu extends Activity implements View.OnClickListener
{

    @Override
    protected void onCreate( Bundle savedInstance )
    {
        super.onCreate( savedInstance );
        setContentView( R.layout.activity_mainmenu );

        final Button btnPlay = ( Button ) findViewById( R.id.btnPlay );
        btnPlay.setOnClickListener( this );

        final Button btnOptions = ( Button ) findViewById( R.id.btnOptions );
        btnOptions.setOnClickListener( this );
    }

    @Override
    public void onClick( View v )
    {
        Intent i;
        switch ( v.getId() )
        {
            case R.id.btnPlay:
                i = new Intent( this, Stage.class );
                break;
            case R.id.btnOptions:
                i = new Intent( this, Options.class );
                break;
            default:
                i = null;
        }
        startActivity( i );
    }
}
