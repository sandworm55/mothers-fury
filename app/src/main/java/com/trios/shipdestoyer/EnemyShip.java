package com.trios.shipdestoyer;

import android.graphics.Bitmap;

import com.trios.shipdestoyer.lib.DrawUtil;
import com.trios.shipdestoyer.lib.Vector2D;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by sandworm on 5/8/2017.
 */

public class EnemyShip implements GameObject
{
    private static Vector2D pos;
    private static int totalHealth;
    private ArrayList< Turret > turrets = new ArrayList<>();
    private ArrayList< int[][] > targets;
    private ArrayList< Phase > phases = new ArrayList<>();
    private int patternSetCurrent, patternSetTotal;
    private ShipTypes type;
    private Bitmap bitmap;
    private int phase;
    private boolean movingToPhase = false;
    private int speed = 1;

    public EnemyShip( ShipTypes type, ArrayList< Turret > turrets, int totalHealth, ArrayList< Phase > phases )
    {
        this.type = type;
        this.phase = 0;
        this.pos = phases.get( phase ).getPos();
        this.turrets = turrets;
        this.phases = phases;
        this.totalHealth = totalHealth;

        switch ( type )
        {
            case GUN_DESTROYER:
                this.bitmap = BitmapLibrary.getBitmap( BitmapLibrary.BitmapFile.GUN_DESTROYER );
                break;
            case MISSILE_DESTROYER:
                this.bitmap = BitmapLibrary.getBitmap( BitmapLibrary.BitmapFile.MISSILE_DESTROYER );
                break;
            case CRUISER:
                this.bitmap = BitmapLibrary.getBitmap( BitmapLibrary.BitmapFile.CRUISER );
                break;
            case PATROL:
                this.bitmap = BitmapLibrary.getBitmap( BitmapLibrary.BitmapFile.PATROL );
                break;
            case CORVETTE:
                this.bitmap = BitmapLibrary.getBitmap( BitmapLibrary.BitmapFile.CORVETTE );
                break;
            case HUNTER:
                this.bitmap = BitmapLibrary.getBitmap( BitmapLibrary.BitmapFile.HUNTER );
                break;
            case BATTLESHIP:
                this.bitmap = BitmapLibrary.getBitmap( BitmapLibrary.BitmapFile.BATTLESHIP );
                break;
            case DREADNOUGHT:
                this.bitmap = BitmapLibrary.getBitmap( BitmapLibrary.BitmapFile.DREADNOUGHT );
                break;
        }

        for ( int i = 0; i < turrets.size(); i++ )
            this.turrets.get( i ).setPos( pos.add( new Vector2D( HardPoint.getHardPoint( type )[ i ].getX() * 480, HardPoint.getHardPoint( type )[ i ].getY() * 800 ) ) );
    }

    public static int getTotalHealth()
    {
        return totalHealth;
    }

    public void nextPhase()
    {
        if ( phases.size() > phase + 1 )
        {
            phase++;
            movingToPhase();
        }
    }

    public ArrayList< Phase > getPhases()
    {
        return phases;
    }

    public ArrayList< Turret > getTurrets()
    {
        return turrets;
    }

    public void movingToPhase()
    {
        movingToPhase = true;
    }

    public void setTargets( int count, PlayerShip playerShip )
    {
        Random random = new Random();

        if ( turrets.size() == 1 )
        {
            getTurrets().get( 0 ).setTarget( playerShip.getShieldGenerators().get( random.nextInt( 4 ) ) );
        }

        if ( turrets.size() == 3 )
        {
            targets = FirePattern.getThreeTurretPatterns( count );

            patternSetTotal = count - 1;
            patternSetCurrent = 0;

            for ( int[] pattern : targets.get( 0 ) )
            {
                getTurrets().get( pattern[ 0 ] ).setTarget( playerShip.getShieldGenerators().get( pattern[ 1 ] ) );
            }
        }
        else if ( turrets.size() >= 4 )
        {
            for ( Turret tur : getTurrets() )
                tur.setTarget( playerShip.getShieldGenerators().get( random.nextInt( 4 ) ) );
        }
    }

    public void changeTargets( PlayerShip playerShip )
    {
        Random random = new Random();
        if ( turrets.size() == 1 )
            getTurrets().get( 0 ).setTarget( playerShip.getShieldGenerators().get( random.nextInt( 4 ) ) );
        else if ( turrets.size() >= 4 )
            for ( Turret tur : getTurrets() )
                tur.setTarget( playerShip.getShieldGenerators().get( random.nextInt( 4 ) ) );
        else
        {
            // temp.get(Pattern)[gun value][shield value]
            patternSetCurrent++;

            if ( patternSetCurrent > patternSetTotal )
                patternSetCurrent = 0;

            for ( int[] pattern : targets.get( patternSetCurrent ) )
            {
                getTurrets().get( pattern[ 0 ] ).setTarget( playerShip.getShieldGenerators().get( pattern[ 1 ] ) );
            }
        }
    }

    @Override
    public void draw()
    {
        DrawUtil.graphic( bitmap, pos, 0 );

        for ( Turret turret : turrets )
            turret.draw();
    }

    @Override
    public void update()
    {
        if ( movingToPhase )
        {
            //if moving left
            if ( pos.getX() == phases.get( phase ).getPos().getX() )
                movingToPhase = false;
            else if ( pos.getX() > phases.get( phase ).getPos().getX() )
                pos.set( pos.getX() - speed, pos.getY() );
            else if ( pos.getX() < phases.get( phase ).getPos().getX() )
                pos.set( pos.getX() + speed, pos.getY() );

            //if moving right
        }

        for ( int i = 0; i < turrets.size(); i++ )
            this.turrets.get( i ).setPos( pos.add( new Vector2D( HardPoint.getHardPoint( type )[ i ].getX() * 480, HardPoint.getHardPoint( type )[ i ].getY() * 800 ) ) );
        for ( Turret turret : turrets )
            turret.update();
    }

    public Vector2D getPos()
    {
        return pos;
    }

    public void setPos( Vector2D pos )
    {
        this.pos = pos;
    }

    public float getCurrentPhaseHealth()
    {
        return phases.get( phase ).getHealthCheck();
    }
}
