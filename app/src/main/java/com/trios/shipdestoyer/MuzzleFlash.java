package com.trios.shipdestoyer;

import android.graphics.Bitmap;

import com.trios.shipdestoyer.lib.DrawUtil;
import com.trios.shipdestoyer.lib.Vector2D;

/**
 * Created by sandworm on 5/11/2017.
 */

public class MuzzleFlash implements GameObject
{
    private static final float PERCENT_PER_FRAME = .02f;
    private Vector2D pos;
    private double rot;

    private float scaleFlash = 0;
    private int opacityFlash = 255;
    private float scaleSmoke = 0;
    private int opacitySmoke = 255;

    private float disX, disY;
    private float killMe = 0;

    private Bitmap bitmapFlash;
    private Bitmap bitmapSmoke;
    private Boolean drawFlash = true;
    private Boolean drawSmoke = true;

    public MuzzleFlash( Vector2D pos, double rot )
    {
        this.pos = pos;
        this.rot = rot;

        bitmapSmoke = BitmapLibrary.getBitmap( BitmapLibrary.BitmapFile.GUN_SMOKE );
        bitmapFlash = BitmapLibrary.getBitmap( BitmapLibrary.BitmapFile.GUN_FLASH );
    }

    public float getKillMe()
    {
        return killMe;
    }

    @Override
    public void draw()
    {

        if ( drawSmoke )
            DrawUtil.graphic( bitmapSmoke, pos.add( new Vector2D( disX, disY ) ), new Vector2D( bitmapSmoke.getWidth() / 2, 0 ), scaleSmoke, ( int ) rot, opacitySmoke );
        if ( drawFlash )
            DrawUtil.graphic( bitmapFlash, pos, new Vector2D( bitmapFlash.getWidth() / 2, 0 ), scaleFlash / 2, ( int ) rot, opacityFlash );

    }

    public void smokeOnly()
    {
        drawFlash = false;
    }

    public void flashOnly()
    {
        drawSmoke = false;
    }

    @Override
    public void update()
    {
        disX -= ( float ) Math.sin( Math.toRadians( rot ) ) * 3f;
        disY += ( float ) Math.cos( Math.toRadians( rot ) ) * 3f;

        scaleSmoke += PERCENT_PER_FRAME;
        opacitySmoke -= 255 * PERCENT_PER_FRAME;

        scaleFlash += PERCENT_PER_FRAME * 2;
        opacityFlash -= 255 * PERCENT_PER_FRAME * 2;

        if ( opacityFlash < 0 )
            opacityFlash = 0;

        if ( opacitySmoke < 0 )
            opacitySmoke = 0;


        killMe += PERCENT_PER_FRAME;
    }
}
