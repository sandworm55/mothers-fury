package com.trios.shipdestoyer;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

/**
 * Created by seand on 5/19/2017.
 */

public class EndGameFail extends Activity implements View.OnClickListener
{
    @Override
    protected void onCreate( Bundle savedInstance )
    {
        super.onCreate( savedInstance );
        setContentView( R.layout.activity_endgamelose );

        final Button btnSelect = ( Button ) findViewById( R.id.btnSelect );
        btnSelect.setOnClickListener( this );

        final Button btnNext = ( Button ) findViewById( R.id.btnRetry );
        btnNext.setOnClickListener( this );
    }

    @Override
    public void onClick( View v )
    {
        Intent i;
        switch ( v.getId() )
        {
            case R.id.btnSelect:
                i = new Intent( this, Stage.class );
                break;
            case R.id.btnRetry:
                i = new Intent( this, GameActivity.class );

                break;
            default:
                i = null;
        }
        startActivity( i );
        finish();
    }


}
