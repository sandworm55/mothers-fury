package com.trios.shipdestoyer;

import android.util.Log;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by sandworm on 5/15/2017.
 */

public class FirePattern
{
    private static int[][][] firePatternThree =
            {
                    { { 0, 0 }, { 1, 1 }, { 2, 2 } },
                    { { 0, 1 }, { 1, 2 }, { 2, 3 } },
                    { { 0, 2 }, { 1, 1 }, { 2, 0 } },
                    { { 0, 3 }, { 1, 2 }, { 2, 1 } },
                    { { 0, 0 }, { 1, 3 }, { 2, 3 } },
            };

    private static int[][][] firePatternSix =
            {
                    { { 0, 0 }, { 1, 1 }, { 2, 2 }, { 3, 1 }, { 4, 2 }, { 5, 3 } },
                    { { 0, 0 }, { 1, 1 }, { 2, 2 }, { 3, 1 }, { 4, 2 }, { 5, 3 } }
            };

    public static ArrayList< int[][] > getThreeTurretPatterns( int numOfPatterns )
    {
        if ( numOfPatterns > firePatternThree.length )
        {
            numOfPatterns = firePatternThree.length - 1;
            Log.e( "** real talk **", "not that many patterns for a three turret ship" );
        }

        ArrayList< Integer > tempCount = new ArrayList<>();
        ArrayList< int[][] > temp = new ArrayList<>();

        for ( int i = 0; i < numOfPatterns; i++ )
        {
            Random rand = new Random();
            int n;

            do
            {
                n = rand.nextInt( firePatternThree.length );
            }
            while ( tempCount.contains( n ) );

            tempCount.add( n );
            temp.add( firePatternThree[ n ] );
        }
        return temp;
    }

    public static ArrayList< int[][] > getSixTurretPatterns( int numOfPatterns )
    {
        ArrayList< int[][] > temp = new ArrayList<>();
        temp.add( firePatternSix[ 0 ] );
        temp.add( firePatternSix[ 1 ] );
        return temp;
    }

    public int[][] getPatternThree( FirePatternThree pattern )
    {
        return firePatternThree[ pattern.ordinal() ];
    }

    public enum FirePatternThree
    {
        STRAIGHT, FAR_SIDE, STRAIGHT_CROSSED, FAR_SIDE_STRAIGHT, ONE_LEFT_TWO_RIGHT
    }
}
