package com.trios.shipdestoyer;

import android.os.Vibrator;

import com.trios.shipdestoyer.lib.SoundManager;
import com.warting.bubbles.ConversationActivity;

import java.util.ArrayList;

import static android.content.Context.VIBRATOR_SERVICE;

/**
 * Created by seand on 5/10/2017.
 */

public class Level implements Runnable
{


    public static int shipHealth = 100;
    public static int playerHealth = 100;
    public static int levelType = 0;
    public static boolean levelEnded;
    public static boolean levelWon;
    ////////////////////////////////
    public EnemyShip enemyShip;
    public PlayerShip playerShip;
    public int count = 0;
    public ArrayList< GameObject > objects = new ArrayList<>();
    volatile boolean playing;

    public Level()
    {
        LevelManager.startLevel( this );
    }

    public static void shipHit( int damage )
    {
        shipHealth -= damage;
    }

    public static void playerHit( int damage )
    {
        playerHealth -= damage;
        Vibrator vibe = ( Vibrator ) View.context.getSystemService( VIBRATOR_SERVICE );
        vibe.vibrate( 50 * damage );
    }

    public EnemyShip getEnemyShip()
    {
        return enemyShip;
    }

    public void setEnemyShip( EnemyShip enemyShip )
    {
        this.enemyShip = enemyShip;
    }

    public PlayerShip getPlayerShip()
    {
        return playerShip;
    }

    public void setPlayerShip( PlayerShip playerShip )
    {
        this.playerShip = playerShip;
    }

    public boolean isLevelEnded()
    {
        return levelEnded;
    }

    public boolean isLevelWon()
    {
        return levelWon;
    }

    public void gameEnded()
    {

    }

    public ArrayList< GameObject > getObjects()
    {
        return objects;
    }

    public void reset()
    {
    }

    public void run()
    {
        while ( playing )
        {
            update();
        }
    }

    public void update()
    {
        //if (levelEnded) return;

        count++;
        if ( count == 500 )
        {
            enemyShip.changeTargets( playerShip );
            count = 0;
        }

        if ( playerHealth <= 0 )
        {
            SoundManager.stopAllSound();
            SoundManager.playSound( SoundManager.DEFEAT, 0, 0, 1 );
            levelEnded = true;
            playing = false;
            playerHealth = 0;
        }

        if ( shipHealth <= 0 )
        {
            SoundManager.stopAllSound();
            SoundManager.playSound( SoundManager.VICTORY, 0, 0, 1 );
            playing = false;
            levelWon = true;
            shipHealth = 0;
            if ( levelType > View.completedLevel )
            {
                View.completedLevel = levelType;
                View.editor.putInt( View.COMPLETED_LEVEL, levelType );
                View.editor.commit();
            }
            switch ( levelType )
            {
                case 1:
                    ConversationActivity.setConvoOnePost();
                    break;
                case 2:
                    ConversationActivity.setConvoTwoPost();
                    break;
                case 3:
                    ConversationActivity.setConvoThreePost();
                    break;
                case 4:
                    ConversationActivity.setConvoFourPost();
                    break;
                case 5:
                    ConversationActivity.setConvoFivePost();
                    break;
                case 6:
                    ConversationActivity.setConvoSixPost();
                    break;
                case 7:
                    ConversationActivity.setConvoSevenPost();
                    break;
                case 8:
                    ConversationActivity.setConvoEightPost();
                    break;
                case 9:
                    ConversationActivity.setConvoNinePost();
                    break;
                case 10:
                    ConversationActivity.setConvoTenPost();
                    break;
                case 11:
                    ConversationActivity.setConvoElevenPost();
                    break;
                case 12:
                    ConversationActivity.setConvoTwelvePost();
                    break;
                case 13:
                    ConversationActivity.setConvoThirteenPost();
                    break;
                case 14:
                    ConversationActivity.setConvoFourteenPost();
                    break;
                case 15:
                    ConversationActivity.setConvoFifteenPost();
                    break;

            }
        }

        if ( ( float ) shipHealth / ( float ) EnemyShip.getTotalHealth() < enemyShip.getCurrentPhaseHealth() )
            enemyShip.nextPhase();
    }
}
