package com.trios.shipdestoyer;

import android.graphics.Bitmap;

import com.trios.shipdestoyer.lib.DrawUtil;
import com.trios.shipdestoyer.lib.Vector2D;

import java.util.ArrayList;

/**
 * Created by sandworm on 5/10/2017.
 */

public class Ballistic extends Turret
{
    private boolean recoil = false, recoilForward = false, recoilBackward = true;
    private int displacement = 0, recoilLength = 10;
    private ArrayList< Bitmap > bitmapBarrels = new ArrayList<>();
    private Bitmap bitmapBody;
    private int barrelLength;

    /**
     * @param facing
     */
    public Ballistic( TurretTypes type, int facing )
    {
        super( type, facing );
        int temp;
        switch ( type )
        {
            case BALLISTIC_SMALL:
                bitmapBody = BitmapLibrary.getBitmap( BitmapLibrary.BitmapFile.SMALL_DESTROYER_TURRET );
                bitmapBarrels.add( BitmapLibrary.getBitmap( BitmapLibrary.BitmapFile.SMALL_DESTROYER_BARREL ) );
                rotationSpeed = .5f;
                projectileSpeed = 1;
                offSet.add( 0 );
                FIRE_RATE = 80;
                damage = 1;
                barrelLength = 32;
                break;
            case BALLISTIC_SMALL_DOUBLE:
                bitmapBody = BitmapLibrary.getBitmap( BitmapLibrary.BitmapFile.SMALL_DOUBLE_TURRET_BODY );
                bitmapBarrels.add( BitmapLibrary.getBitmap( BitmapLibrary.BitmapFile.SMALL_DOUBLE_TURRET_BARREL_1 ) );
                bitmapBarrels.add( BitmapLibrary.getBitmap( BitmapLibrary.BitmapFile.SMALL_DOUBLE_TURRET_BARREL_2 ) );
                rotationSpeed = .5f;
                projectileSpeed = 2;
                temp = 5;
                offSet.add( -temp );
                offSet.add( temp );
                FIRE_RATE = 80;
                damage = 2;
                barrelLength = 42;
                break;
            case BALLISTIC_LARGE:
                bitmapBody = BitmapLibrary.getBitmap( BitmapLibrary.BitmapFile.LARGE_DESTROYER_TURRET_BODY );
                bitmapBarrels.add( BitmapLibrary.getBitmap( BitmapLibrary.BitmapFile.LARGE_DESTROYER_BARREL ) );
                rotationSpeed = .1f;
                projectileSpeed = 2;
                offSet.add( 0 );
                FIRE_RATE = 120;
                damage = 3;
                barrelLength = 62;
                break;
            case BALLISTIC_CRUISER:
                bitmapBody = BitmapLibrary.getBitmap( BitmapLibrary.BitmapFile.CRUISER_BATTLESHIP_TURRET_BODY );
                bitmapBarrels.add( BitmapLibrary.getBitmap( BitmapLibrary.BitmapFile.CRUISER_BATTLESHIP_BARREL_1 ) );
                bitmapBarrels.add( BitmapLibrary.getBitmap( BitmapLibrary.BitmapFile.CRUISER_BATTLESHIP_BARREL_2 ) );
                bitmapBarrels.add( BitmapLibrary.getBitmap( BitmapLibrary.BitmapFile.CRUISER_BATTLESHIP_BARREL_3 ) );
                rotationSpeed = .05f;
                projectileSpeed = 2;
                temp = 25;
                offSet.add( -temp );
                offSet.add( 0 );
                offSet.add( temp );
                FIRE_RATE = 200;
                damage = 3;
                barrelLength = 93;
                break;
            default:
                bitmapBody = BitmapLibrary.getBitmap( BitmapLibrary.BitmapFile.SMALL_DESTROYER_TURRET );
                bitmapBarrels.add( BitmapLibrary.getBitmap( BitmapLibrary.BitmapFile.SMALL_DESTROYER_BARREL ) );
                damage = 100;
        }
    }

    @Override
    public void draw()
    {
        super.draw();
        if ( pos == null )
            return;

        double displacementX = ( Math.sin( Math.toRadians( facing ) ) * displacement );
        double displacementY = ( Math.cos( Math.toRadians( facing ) ) * displacement );

        //barrel
        for ( Bitmap bitmapBarrel : bitmapBarrels )
        {
            recoilLength = ( int ) ( bitmapBarrel.getHeight() * .1f );
            DrawUtil.graphic( bitmapBarrel, pos.add( new Vector2D( displacementX, -displacementY ) ), new Vector2D( bitmapBarrel.getWidth() / 2, 0 ), ( int ) facing );
        }

        //body
        DrawUtil.graphic( bitmapBody, pos, ( int ) facing );
    }

    @Override
    public void update()
    {
        super.update();

        recoilBarrel();
    }

    @Override
    public Boolean fireBullet()
    {
        if ( !lockedOn )
            return false;
        currentFireRate = 0;
        for ( int shot :
                offSet )
        {
            double disX = ( Math.sin( Math.toRadians( facing ) ) * shot );
            double disY = ( Math.cos( Math.toRadians( facing ) ) * shot );

            double barX = ( Math.sin( Math.toRadians( facing ) ) * barrelLength );
            double barY = ( Math.cos( Math.toRadians( facing ) ) * barrelLength );
            projectiles.add( new Projectile( this.getPos().add( new Vector2D( disY, disX ) ).add( new Vector2D( -barX, barY ) ),
                                             target, projectileSpeed, this.getType(), damage ) );
        }


        recoil = true;

        return true;
    }

    private void recoilBarrel()
    {
        if ( recoil )
        {
            if ( displacement >= recoilLength )
            {
                recoilBackward = false;
                recoilForward = true;
            }

            if ( recoilBackward )
                displacement += 5;

            if ( recoilForward )
            {
                displacement--;
                if ( displacement <= 0 )
                {
                    recoilBackward = true;
                    recoilForward = false;
                    recoil = false;
                }
            }
        }
    }
}
