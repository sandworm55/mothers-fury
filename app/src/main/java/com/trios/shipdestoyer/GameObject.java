package com.trios.shipdestoyer;

/**
 * Created by sandworm on 5/8/2017.
 */

public interface GameObject
{
    float SCALE = 0.8f;

    void draw();

    void update();
}
