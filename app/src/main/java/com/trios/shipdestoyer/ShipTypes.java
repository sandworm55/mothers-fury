package com.trios.shipdestoyer;

/**
 * Created by sandworm on 5/8/2017.
 */

public enum ShipTypes
{

    GUN_DESTROYER, MISSILE_DESTROYER, CRUISER, PATROL, CORVETTE, HUNTER, BATTLESHIP, DREADNOUGHT,
}
