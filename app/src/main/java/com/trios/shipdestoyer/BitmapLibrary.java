package com.trios.shipdestoyer;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.util.Log;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by sandworm on 5/12/2017.
 */

public class BitmapLibrary
{
    private static Bitmap[] bitmaps = new Bitmap[ 77 ];
    private static ArrayList< Bitmap > stars = new ArrayList<>();
    private static ArrayList< Bitmap > planets = new ArrayList<>();
    private static ArrayList< Bitmap > nebulas = new ArrayList<>();
    private static Canvas canvas;

    public static void init( Context context )
    {
        addBitmap( context, R.drawable.battleship, BitmapFile.BATTLESHIP );
        addBitmap( context, R.drawable.corvette, BitmapFile.CORVETTE );
        addBitmap( context, R.drawable.cruiser, BitmapFile.CRUISER );
        addBitmap( context, R.drawable.dreadnought, BitmapFile.DREADNOUGHT );
        addBitmap( context, R.drawable.patrol_ship, BitmapFile.PATROL );
        addBitmap( context, R.drawable.gun_destroyer, BitmapFile.GUN_DESTROYER );
        addBitmap( context, R.drawable.missile_destroyer, BitmapFile.MISSILE_DESTROYER );
        addBitmap( context, R.drawable.ship_hunter, BitmapFile.HUNTER );
        addBitmap( context, R.drawable.player_ship, BitmapFile.PLAYER_SHIP );

        addBitmap( context, R.drawable.cruiser_battleship_turret_body, BitmapFile.CRUISER_BATTLESHIP_TURRET_BODY );
        addBitmap( context, R.drawable.cruiser_battleship_barrel_1, BitmapFile.CRUISER_BATTLESHIP_BARREL_1 );
        addBitmap( context, R.drawable.cruiser_battleship_barrel_2, BitmapFile.CRUISER_BATTLESHIP_BARREL_2 );
        addBitmap( context, R.drawable.cruiser_battleship_barrel_3, BitmapFile.CRUISER_BATTLESHIP_BARREL_3 );

        addBitmap( context, R.drawable.large_destroyer_turret_body, BitmapFile.LARGE_DESTROYER_TURRET_BODY );
        addBitmap( context, R.drawable.large_destroyer_barrel, BitmapFile.LARGE_DESTROYER_BARREL );

        addBitmap( context, R.drawable.small_double_turret_body, BitmapFile.SMALL_DOUBLE_TURRET_BODY );
        addBitmap( context, R.drawable.small_double_turret_barrel_1, BitmapFile.SMALL_DOUBLE_TURRET_BARREL_1 );
        addBitmap( context, R.drawable.small_double_turret_barrel_2, BitmapFile.SMALL_DOUBLE_TURRET_BARREL_2 );

        addBitmap( context, R.drawable.small_destroyer_turret, BitmapFile.SMALL_DESTROYER_TURRET );
        addBitmap( context, R.drawable.small_destroyer_barrel, BitmapFile.SMALL_DESTROYER_BARREL );

        addBitmap( context, R.drawable.small_missile_turret, BitmapFile.SMALL_MISSILE_TURRET );
        addBitmap( context, R.drawable.large_missile_turret, BitmapFile.LARGE_MISSILE_TURRET );
        addBitmap( context, R.drawable.small_verticle_missile, BitmapFile.SMALL_VERTICAL_MISSILE );
        addBitmap( context, R.drawable.mini_rocket_turret, BitmapFile.MINI_ROCKET_TURRET );

        addBitmap( context, R.drawable.super_laser_turret, BitmapFile.SUPER_LASER_TURRET );
        addBitmap( context, R.drawable.super_laser_nodes, BitmapFile.SUPER_LASER_NODES );
        addBitmap( context, R.drawable.super_laser_vains, BitmapFile.SUPER_LASER_VEINS );
        addBitmap( context, R.drawable.super_laser_flash_1, BitmapFile.SUPER_LASER_FLASH1 );
        addBitmap( context, R.drawable.super_laser_flash_2, BitmapFile.SUPER_LASER_FLASH2 );
        addBitmap( context, R.drawable.super_laser_flash_3, BitmapFile.SUPER_LASER_FLASH3 );
        addBitmap( context, R.drawable.super_laser_flash_4, BitmapFile.SUPER_LASER_FLASH4 );
        addBitmap( context, R.drawable.super_laser_flash_5, BitmapFile.SUPER_LASER_FLASH5 );
        addBitmap( context, R.drawable.super_laser_flash_6, BitmapFile.SUPER_LASER_FLASH6 );
        addBitmap( context, R.drawable.super_laser_flash_7, BitmapFile.SUPER_LASER_FLASH7 );

        addBitmap( context, R.drawable.massive_laser_turret, BitmapFile.MASSIVE_LASER_TURRET );
        addBitmap( context, R.drawable.massive_laser_flash, BitmapFile.MASSIVE_LASER_FLASH );

        addBitmap( context, R.drawable.large_laser_turret, BitmapFile.LARGE_LASER_TURRET );
        addBitmap( context, R.drawable.large_laser_flash, BitmapFile.LARGE_LASER_FLASH );

        addBitmap( context, R.drawable.medium_laser_turret, BitmapFile.MEDIUM_LASER_TURRET );
        addBitmap( context, R.drawable.medium_laser_flash, BitmapFile.MEDIUM_LASER_FLASH );

        addBitmap( context, R.drawable.small_laser_turret, BitmapFile.SMALL_LASER_TURRET );
        addBitmap( context, R.drawable.small_laser_flash, BitmapFile.SMALL_LASER_FLASH );

        addBitmap( context, R.drawable.laser, BitmapFile.LASER );
        addBitmap( context, R.drawable.shell, BitmapFile.SHELL );
        addBitmap( context, R.drawable.missile, BitmapFile.MISSILE );

        addBitmap( context, R.drawable.shield_sparks, BitmapFile.SHIELD_SPARKS );
        addBitmap( context, R.drawable.small_explosion, BitmapFile.SMALL_EXPLOSION );
        addBitmap( context, R.drawable.shield, BitmapFile.SHIELD );
        addBitmap( context, R.drawable.gun_flash, BitmapFile.GUN_FLASH );
        addBitmap( context, R.drawable.gun_smoke, BitmapFile.GUN_SMOKE );
        addBitmap( context, R.drawable.shield_generator, BitmapFile.SHIELD_GENERATOR );

        addBitmap( context, R.drawable.desert_planet, BitmapFile.DESERT_PLANET );
        addBitmap( context, R.drawable.gas_planet, BitmapFile.GAS_PLANET );
        addBitmap( context, R.drawable.tarren_planet, BitmapFile.TARREN_PLANET );

        planets.add( getBitmap( BitmapFile.DESERT_PLANET ) );
        planets.add( getBitmap( BitmapFile.GAS_PLANET ) );
        planets.add( getBitmap( BitmapFile.TARREN_PLANET ) );

        addBitmap( context, R.drawable.nebula1_green, BitmapFile.NEBULA_GREEN );
        addBitmap( context, R.drawable.nebula1_red, BitmapFile.NEBULA_RED );
        addBitmap( context, R.drawable.nebula2_pink, BitmapFile.NEBULA_PINK );
        addBitmap( context, R.drawable.nebula2_purple, BitmapFile.NEBULA_PURPLE );
        addBitmap( context, R.drawable.nebula1_lightblue, BitmapFile.NEBULA_LIGHT_BLUE );
        addBitmap( context, R.drawable.nebula2_lightgreen, BitmapFile.NEBULA_LIGHT_GREEN );

        nebulas.add( getBitmap( BitmapFile.NEBULA_GREEN ) );
        nebulas.add( getBitmap( BitmapFile.NEBULA_RED ) );
        nebulas.add( getBitmap( BitmapFile.NEBULA_PINK ) );
        nebulas.add( getBitmap( BitmapFile.NEBULA_PURPLE ) );
        nebulas.add( getBitmap( BitmapFile.NEBULA_LIGHT_BLUE ) );
        nebulas.add( getBitmap( BitmapFile.NEBULA_LIGHT_GREEN ) );

        addBitmap( context, R.drawable.star_large_blue, BitmapFile.STAR_LARGE_BLUE_1 );
        addBitmap( context, R.drawable.star_large_blue_2, BitmapFile.STAR_LARGE_BLUE_2 );
        addBitmap( context, R.drawable.star_large_green, BitmapFile.STAR_LARGE_GREEN );
        addBitmap( context, R.drawable.star_large_red, BitmapFile.STAR_LARGE_RED );
        addBitmap( context, R.drawable.star_pulsar, BitmapFile.STAR_PULSAR );
        addBitmap( context, R.drawable.star_pulsar_plasma, BitmapFile.STAR_PULSAR_PLASMA );
        addBitmap( context, R.drawable.star_small_orange, BitmapFile.STAR_SMALL_ORANGE );
        addBitmap( context, R.drawable.star_tiny_red, BitmapFile.STAR_TINY_RED );
        addBitmap( context, R.drawable.star_tiny_white, BitmapFile.STAR_TINY_WHITE );

        stars.add( getBitmap( BitmapFile.STAR_LARGE_BLUE_1 ) );
        stars.add( getBitmap( BitmapFile.STAR_LARGE_BLUE_2 ) );
        stars.add( getBitmap( BitmapFile.STAR_LARGE_GREEN ) );
        stars.add( getBitmap( BitmapFile.STAR_LARGE_RED ) );
        stars.add( getBitmap( BitmapFile.STAR_PULSAR ) );
        stars.add( getBitmap( BitmapFile.STAR_SMALL_ORANGE ) );
        stars.add( getBitmap( BitmapFile.STAR_TINY_RED ) );
        stars.add( getBitmap( BitmapFile.STAR_TINY_WHITE ) );

        Log.d( "reload", "Bitmaps loaded. " + bitmaps.length );
    }

    public static void addBitmap( Context context, int rValue, BitmapFile bitmapFile )
    {
        Bitmap bitmap = BitmapFactory.decodeResource( context.getResources(), rValue );
        bitmaps[ bitmapFile.ordinal() ] = bitmap;
    }

    public static Canvas getCanvas()
    {
        return canvas;
    }

    public static void setCanvas( Canvas canvas )
    {
        if ( BitmapLibrary.canvas == null )
            BitmapLibrary.canvas = canvas;
    }

    public static void clearCanvas()
    {
        BitmapLibrary.canvas = null;
    }

    public static Bitmap getBitmap( BitmapFile bitmapFile )
    {
        return bitmaps[ bitmapFile.ordinal() ];
    }

    public static Bitmap getStar()
    {
        Random random = new Random();
        return stars.get( random.nextInt( stars.size() ) );
    }


    public static Bitmap getNebula()
    {
        Random random = new Random();
        return nebulas.get( random.nextInt( nebulas.size() ) );
    }


    public static Bitmap getPlanet()
    {
        Random random = new Random();
        return planets.get( random.nextInt( planets.size() ) );
    }

    public static void destroy()
    {
        bitmaps = new Bitmap[ 77 ];
        stars = new ArrayList<>();
        planets = new ArrayList<>();
        nebulas = new ArrayList<>();
    }

    public enum BitmapFile
    {
        // Ships
        PLAYER_SHIP, MISSILE_DESTROYER, GUN_DESTROYER, CRUISER, PATROL, CORVETTE, HUNTER, BATTLESHIP, DREADNOUGHT,

        //Turrets------------------------------------------------------
        //Ballistic turrets
        CRUISER_BATTLESHIP_TURRET_BODY, CRUISER_BATTLESHIP_BARREL_1, CRUISER_BATTLESHIP_BARREL_2, CRUISER_BATTLESHIP_BARREL_3,
        LARGE_DESTROYER_TURRET_BODY, LARGE_DESTROYER_BARREL,
        SMALL_DOUBLE_TURRET_BODY, SMALL_DOUBLE_TURRET_BARREL_1, SMALL_DOUBLE_TURRET_BARREL_2,
        SMALL_DESTROYER_BARREL, SMALL_DESTROYER_TURRET,

        //Missiles
        SMALL_MISSILE_TURRET, LARGE_MISSILE_TURRET, SMALL_VERTICAL_MISSILE, MINI_ROCKET_TURRET,

        //Lasers
        SUPER_LASER_TURRET, SUPER_LASER_NODES, SUPER_LASER_VEINS, SUPER_LASER_FLASH1, SUPER_LASER_FLASH2, SUPER_LASER_FLASH3, SUPER_LASER_FLASH4, SUPER_LASER_FLASH5, SUPER_LASER_FLASH6, SUPER_LASER_FLASH7,
        MASSIVE_LASER_TURRET, MASSIVE_LASER_FLASH,
        LARGE_LASER_TURRET, LARGE_LASER_FLASH,
        MEDIUM_LASER_TURRET, MEDIUM_LASER_FLASH,
        SMALL_LASER_TURRET, SMALL_LASER_FLASH,

        //Projectiles
        LASER, SHELL, MISSILE,

        //Effects
        SHIELD_SPARKS, SMALL_EXPLOSION, SHIELD, GUN_FLASH, GUN_SMOKE, SHIELD_GENERATOR,

        //Backgrounds-------------------------------------
        //Planets
        DESERT_PLANET, GAS_PLANET, TARREN_PLANET,

        //Nebulas
        NEBULA_GREEN, NEBULA_RED, NEBULA_LIGHT_BLUE, NEBULA_LIGHT_GREEN, NEBULA_PINK, NEBULA_PURPLE,

        //Stars
        STAR_LARGE_BLUE_1, STAR_LARGE_BLUE_2, STAR_LARGE_GREEN, STAR_LARGE_RED, STAR_PULSAR, STAR_PULSAR_PLASMA, STAR_SMALL_ORANGE, STAR_TINY_RED, STAR_TINY_WHITE,
    }

}
