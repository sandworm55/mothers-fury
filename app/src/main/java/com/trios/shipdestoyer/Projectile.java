package com.trios.shipdestoyer;

import android.graphics.Bitmap;

import com.trios.shipdestoyer.lib.DrawUtil;
import com.trios.shipdestoyer.lib.SoundManager;
import com.trios.shipdestoyer.lib.Vector2D;

import java.util.Random;

/**
 * Created by Stewart on 2017-05-01.
 */

public class Projectile implements GameObject
{
    private Vector2D start, end, pos;
    private int damage;
    private double rot, speed;
    private TurretTypes type;
    private MuzzleFlash flash;
    private Bitmap bitmap;
    private Bitmap bitmapExplosion;
    private Bitmap bitmapSparks;
    private ShieldGenerator target;

    private boolean deflected = false;
    private boolean laser = false;
    private boolean missile = false;
    private boolean hit = false;
    private boolean block = false;

    /**
     * A projectile
     *
     * @param source        the position of it's starting pos
     * @param target        the position of it's target pos
     * @param movementSpeed it's speed
     * @param type          the type of projectile
     */
    public Projectile( Vector2D source, ShieldGenerator target, double movementSpeed, TurretTypes type, int damage )
    {
        this.start = source;
        this.target = target;
        Random rand = new Random();
        this.end = target.getPos().add( new Vector2D( rand.nextInt( target.getRadius() ) - target.getRadius() / 2, rand.nextInt( target.getRadius() ) - target.getRadius() / 2 ) );

        this.pos = start;

        speed = movementSpeed;

        this.type = type;
        this.damage = damage;

        rot = end.subtract( start ).getRotation();

        flash = new MuzzleFlash( pos, rot );


        bitmapExplosion = BitmapLibrary.getBitmap( BitmapLibrary.BitmapFile.SMALL_EXPLOSION );

        bitmapSparks = BitmapLibrary.getBitmap( BitmapLibrary.BitmapFile.SHIELD_SPARKS );

        switch ( type )
        {
            case BALLISTIC_SMALL:
            case BALLISTIC_SMALL_DOUBLE:
            case BALLISTIC_LARGE:
            case BALLISTIC_CRUISER:
                SoundManager.playSound( SoundManager.BALLISTIC_FIRING, 0, 0, 1 );
                bitmap = BitmapLibrary.getBitmap( BitmapLibrary.BitmapFile.SHELL );
                break;
            case SMALL_MISSILE:
            case LARGE_MISSILE:
            case MINI_ROCKET:
                SoundManager.playSound( SoundManager.MISSILE_FIRING, 0, 0, 1 );
                bitmap = BitmapLibrary.getBitmap( BitmapLibrary.BitmapFile.MISSILE );
                missile = true;
                flash.smokeOnly();
                break;
            case SMALL_LASER:
            case MEDIUM_LASER:
            case LARGE_LASER:
            case MASSIVE_LASER:
            case SUPER_LASER:
                SoundManager.playSound( SoundManager.LASER_FIRING, 0, 0, 1 );
                bitmap = BitmapLibrary.getBitmap( BitmapLibrary.BitmapFile.LASER );
                laser = true;
                flash.flashOnly();
                break;
            default:
                bitmap = BitmapLibrary.getBitmap( BitmapLibrary.BitmapFile.SHIELD );
                laser = false;
        }
    }

    public double getRot()
    {
        return rot;
    }

    public void setRot( double rot )
    {
        this.rot = rot;
    }

    public TurretTypes getType()
    {
        return type;
    }

    @Override
    public void draw()
    {
        DrawUtil.graphic( this.bitmap, pos, 0.5f, ( int ) rot );

        if ( flash.getKillMe() <= 1f )
            flash.draw();

    }

    @Override
    public void update()
    {
        Vector2D dist = end.subtract( start );
        double angle = dist.getRotation();

        if ( getEnd().getY() < getStart().getY() )
            angle += 180;

        Vector2D travelDis;

        if ( deflected )
            travelDis = dist.projection( new Vector2D( getSpeed() * 7, -getSpeed() * 7 ) );
        else
            travelDis = dist.projection( new Vector2D( getSpeed() * 7, getSpeed() * 7 ) );

        setPos( getPos().add( travelDis ) );
        setRot( angle );

        if ( flash.getKillMe() <= 1f )
            flash.update();
    }

    public Vector2D getEnd()
    {
        return end;
    }

    public void setEnd( Vector2D end )
    {
        this.end = end;
    }

    public Vector2D getStart()
    {
        return start;
    }

    public void setStart( Vector2D start )
    {
        this.start = start;
    }

    public double getSpeed()
    {
        return speed;
    }

    public Vector2D getPos()
    {
        return pos;
    }

    public void setPos( Vector2D pos )
    {
        this.pos = pos;
    }

    public HitEffect hitTest()
    {

        Vector2D dist = end.subtract( pos );
        if ( !deflected )
        {
            if ( dist.getMagnitude() < target.getRadius() )
            {
                //hit

                Level.playerHit( damage );
                hit = true;

                return new HitEffect( HitEffect.Type.EXPLOSION, pos );
            }
            else if ( target.isActive() && dist.getMagnitude() <= target.getRadius() + 80 && dist.getMagnitude() >= target.getRadius() + 40 )
            {
                //block

                if ( missile )
                {
                    Level.playerHit( damage / 3 );

                    return new HitEffect( HitEffect.Type.EXPLOSION, pos );
                }
                else
                {
                    return new HitEffect( HitEffect.Type.SPARK, pos );
                }

            }
            else if ( target.isActive() && dist.getMagnitude() < target.getRadius() + 40 )
            {

                //deflect

                if ( !laser )
                {
                    SoundManager.playSound( SoundManager.SHIELD_DEFLECT, 0, 0, 1 );
                    deflected = true;

                    Random rand = new Random();
                    int n = rand.nextInt( 440 ) + 40;
                    int m = rand.nextInt( 30 ) + 10;
                    end = ( new Vector2D( n, m ) );

                    setStart( getPos() );
                }
                else
                {
                    return new HitEffect( HitEffect.Type.SPARK, pos );
                }
            }
        }

        if ( deflected )
        {
            if ( end.subtract( pos ).getMagnitude() <= 15 )//this number is because if it's 0, it will never be = to 0, you can't be negative distance from something
            {
                Level.shipHit( damage );

                hit = true;

                return new HitEffect( HitEffect.Type.EXPLOSION, pos );
            }
        }

        return null;
    }
}
