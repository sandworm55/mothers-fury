package com.trios.shipdestoyer;

import android.os.Build;
import android.support.annotation.RequiresApi;

import com.trios.shipdestoyer.lib.DrawUtil;
import com.trios.shipdestoyer.lib.Vector2D;

import java.util.ArrayList;

/**
 * Created by sandworm on 5/8/2017.
 */

public class PlayerShip implements GameObject
{
    private static int totalHealth;
    private ArrayList< ShieldGenerator > shieldGenerators;
    private Vector2D pos;

    public PlayerShip( Vector2D pos, ArrayList< ShieldGenerator > shieldGenerators, int totalHealth )
    {
        this.pos = pos;
        this.shieldGenerators = shieldGenerators;
        this.totalHealth = totalHealth;
    }

    public static int getTotalHealth()
    {
        return totalHealth;
    }

    public ArrayList< ShieldGenerator > getShieldGenerators()
    {
        return shieldGenerators;
    }

    @RequiresApi( api = Build.VERSION_CODES.LOLLIPOP )
    @Override
    public void draw()
    {
        DrawUtil.graphic( BitmapLibrary.getBitmap( BitmapLibrary.BitmapFile.PLAYER_SHIP ), pos, SCALE, 0 );
        for ( ShieldGenerator gen : shieldGenerators )
            gen.draw();
    }

    public Vector2D getPos()
    {
        return pos;
    }

    @Override
    public void update()
    {
        for ( ShieldGenerator gen : shieldGenerators )
            gen.update();
    }
}
