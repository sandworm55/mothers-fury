package com.trios.shipdestoyer.lib;


public class Vector2D
{

    private double x, y;

    public Vector2D( double xLoc, double yLoc )
    {
        x = xLoc;
        y = yLoc;
    }

    public void set( double xLoc, double yLoc )
    {
        x = xLoc;
        y = yLoc;
    }

    public Vector2D add( Vector2D vector2 )
    {
        return new Vector2D( x + vector2.x, y + vector2.y );
    }

    public Vector2D subtract( Vector2D vector2 )
    {
        return new Vector2D( x - vector2.x, y - vector2.y );
    }

    public Vector2D speedTo( double scale )
    {
        return normalize( this ).multiply( scale );
    }

    public Vector2D multiply( double scale )
    {
        return new Vector2D( x * scale, y * scale );
    }

    public Vector2D normalize( Vector2D vector2 )
    {
        return new Vector2D( x / vector2.getMagnitude(), y / vector2.getMagnitude() );
    }

    public double getMagnitude()
    {
        return Math.sqrt( x * x + y * y );
    }

    public double getRotation()
    {
        return ( Math.atan( x / -y ) ) * 180 / Math.PI;
        // (* 180 / Math.PI) to be replaced with to toDegree function
    }

    public Vector2D projection( Vector2D vector2 )
    {
        return multiply( dotProduct( vector2 ) / ( getMagnitude() * getMagnitude() ) );
    }

    public double dotProduct( Vector2D vector2 )
    {
        return x * vector2.x + y * vector2.y;
    }

    ////
    public String print()
    {
        return "X : " + this.x + "   Y : " + this.y;
    }

    public double getX()
    {
        return x;
    }

    public double getY()
    {
        return y;
    }

    public Vector2D clone()
    {
        double tempX = x, tempY = y;
        return new Vector2D( tempX, tempY );
    }
}
