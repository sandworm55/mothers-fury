package com.trios.shipdestoyer.lib;

import android.graphics.Canvas;

/**
 * Created by sandw on 5/1/2017.
 */

public final class MathFunc
{

    //    public < T > double getPercent( T x, T y, boolean rounded )
    //    {
    //        if ( ( double ) y == 0 )
    //        {
    //            Log.e( " ** LIB ** ", "Don't divide by 0" );
    //            return 0;
    //        }
    //        if ( rounded ) return Math.floor( ( double ) x / ( double ) y );
    //        else return ( ( double ) x / ( double ) y );
    //    }

    public static int convertX( double x, Canvas canvas )
    {
        double temp = ( double ) canvas.getWidth() / ( double ) 480;
        return Math.round( ( float ) ( temp * x ) );
    }

    public static int convertY( double y, Canvas canvas )
    {
        double temp = ( double ) canvas.getHeight() / ( double ) 800;
        return Math.round( ( float ) ( temp * y ) );
    }

    public static int convertXFrom( double x, int width )
    {
        double temp = ( double ) width / ( double ) 480;
        return ( int ) ( x / temp );
    }

    public static int convertYFrom( double y, int height )
    {
        double temp = ( double ) height / ( double ) 800;
        return ( int ) ( y / temp );
    }
}
