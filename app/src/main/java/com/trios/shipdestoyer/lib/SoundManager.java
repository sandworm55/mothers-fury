package com.trios.shipdestoyer.lib;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Build;
import android.util.Log;

import com.trios.shipdestoyer.Options;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Stewart on 2017-05-19.
 */

public class SoundManager
{
    //guns
    public static int EXPLOSION = -1;
    public static int BALLISTIC_FIRING = -1;
    public static int MISSILE_FIRING = -1;
    public static int LASER_CHARGING = -1;
    public static int LASER_FIRING = -1;
    public static int TURRET_TURNING = -1;
    //shield
    public static int SHIELD_HIT = -1;
    public static int SHIELD_HUM = -1;
    public static int SHIELD_DEFLECT = -1;
    //game
    public static int VICTORY = -1;
    public static int DEFEAT = -1;
    private static SoundPool soundPool;
    private static ArrayList< Integer > ids = new ArrayList<>();


    public static void init( Context context )
    {
        if ( Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP )
        {
            soundPool = new SoundPool.Builder().setMaxStreams( 30 ).build();
        }
        else
        {
            soundPool = new SoundPool( 30, AudioManager.STREAM_MUSIC, 0 );
        }

        try
        {
            AssetManager assetManager = context.getAssets();
            AssetFileDescriptor descriptor;

            descriptor = assetManager.openFd( "explosion.ogg" );
            EXPLOSION = soundPool.load( descriptor, 0 );

            descriptor = assetManager.openFd( "ballistic_firing.ogg" );
            BALLISTIC_FIRING = soundPool.load( descriptor, 0 );

            descriptor = assetManager.openFd( "missile_firing.ogg" );
            MISSILE_FIRING = soundPool.load( descriptor, 0 );

            descriptor = assetManager.openFd( "laser_charging.ogg" );
            LASER_CHARGING = soundPool.load( descriptor, 0 );

            descriptor = assetManager.openFd( "laser_firing.ogg" );
            LASER_FIRING = soundPool.load( descriptor, 0 );

            descriptor = assetManager.openFd( "turret_turning.ogg" );
            TURRET_TURNING = soundPool.load( descriptor, 0 );

            descriptor = assetManager.openFd( "shield_hit.ogg" );
            SHIELD_HIT = soundPool.load( descriptor, 0 );

            descriptor = assetManager.openFd( "shield_hum.ogg" );
            SHIELD_HUM = soundPool.load( descriptor, 0 );

            descriptor = assetManager.openFd( "shield_deflect.ogg" );
            SHIELD_DEFLECT = soundPool.load( descriptor, 0 );

            descriptor = assetManager.openFd( "victory.ogg" );
            VICTORY = soundPool.load( descriptor, 0 );

            descriptor = assetManager.openFd( "defeat.ogg" );
            DEFEAT = soundPool.load( descriptor, 0 );

        } catch ( IOException e )
        {
            Log.e( "ERROR", "Failed to load sound files: " + e.getMessage() );
        }
    }

    public static int playSound( int name, int priority, int loop, float rate )
    {
        int temp = soundPool.play( name, Options.getSfxVolume() / 10, Options.getSfxVolume() / 10, priority, loop, rate );
        if ( loop != 0 )
            ids.add( temp );
        return temp;


    }

    public static int playSound( float volume, int name, int priority, int loop, float rate )
    {
        int temp = soundPool.play( name, Options.getSfxVolume() / 10 + volume, Options.getSfxVolume() / 10 + volume, priority, loop, rate );
        if ( loop != 0 )
            ids.add( temp );
        return temp;
    }

    public static void stopSound( Integer streamID )
    {
        soundPool.stop( streamID );
        ids.remove( streamID );
    }

    public static void stopAllSound()
    {
        for ( Integer streamID : ids )
        {
            soundPool.stop( streamID );
        }
        ids.clear();
    }

}
