package com.trios.shipdestoyer.lib;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.os.Build;
import android.support.annotation.RequiresApi;

import com.trios.shipdestoyer.BitmapLibrary;

/**
 * Created by Stewart on 2017-05-10.
 */

public class DrawUtil
{

    public static final void graphic( Bitmap bitmap, Vector2D pos, int rotation )
    {
        Matrix matrix = new Matrix();

        matrix.setRotate( rotation, bitmap.getWidth() / 2, bitmap.getHeight() / 2 );

        int newX = ( int ) ( MathFunc.convertX( pos.getX(), BitmapLibrary.getCanvas() ) - ( ( double ) bitmap.getWidth() / ( double ) 2 ) );
        int newY = ( int ) ( MathFunc.convertY( pos.getY(), BitmapLibrary.getCanvas() ) - ( ( double ) bitmap.getHeight() / ( double ) 2 ) );

        matrix.postTranslate( newX, newY );

        BitmapLibrary.getCanvas().drawBitmap( bitmap, matrix, new Paint() );
    }

    public static final void graphic( Bitmap bitmap, Vector2D pos, int rotation,
                                      int opacity )
    {
        Matrix matrix = new Matrix();

        matrix.setRotate( rotation, bitmap.getWidth() / 2, bitmap.getHeight() / 2 );

        int newX = ( int ) ( MathFunc.convertX( pos.getX(), BitmapLibrary.getCanvas() ) - ( ( double ) bitmap.getWidth() / ( double ) 2 ) );
        int newY = ( int ) ( MathFunc.convertY( pos.getY(), BitmapLibrary.getCanvas() ) - ( ( double ) bitmap.getHeight() / ( double ) 2 ) );

        matrix.postTranslate( newX, newY );
        Paint paint = new Paint();
        paint.setAlpha( opacity );
        BitmapLibrary.getCanvas().drawBitmap( bitmap, matrix, paint );
    }

    public static final void graphic( Bitmap bitmap, Vector2D pos,
                                      Vector2D pivotPoint, int rotation )
    {
        Matrix matrix = new Matrix();

        matrix.setRotate( rotation, ( int ) pivotPoint.getX(), ( int ) pivotPoint.getY() );

        int newX = ( int ) ( MathFunc.convertX( pos.getX(), BitmapLibrary.getCanvas() ) - pivotPoint.getX() );
        int newY = ( int ) ( MathFunc.convertY( pos.getY(), BitmapLibrary.getCanvas() ) - pivotPoint.getY() );

        matrix.postTranslate( newX, newY );

        BitmapLibrary.getCanvas().drawBitmap( bitmap, matrix, new Paint() );
    }

    public static final void graphic( Bitmap bitmap, Vector2D pos,
                                      Vector2D pivotPoint, float scale, int rotation )
    {
        Matrix matrix = new Matrix();

        matrix.setRotate( rotation, ( int ) pivotPoint.getX(), ( int ) pivotPoint.getY() );
        matrix.postScale( scale, scale, ( float ) pivotPoint.getX(), ( float ) pivotPoint.getY() );

        int newX = ( int ) ( MathFunc.convertX( pos.getX(), BitmapLibrary.getCanvas() ) - pivotPoint.getX() );
        int newY = ( int ) ( MathFunc.convertY( pos.getY(), BitmapLibrary.getCanvas() ) - pivotPoint.getY() );

        matrix.postTranslate( newX, newY );

        BitmapLibrary.getCanvas().drawBitmap( bitmap, matrix, new Paint() );
    }


    public static final void graphic( Bitmap bitmap, Vector2D pos,
                                      float scale, int rotation )
    {
        Matrix matrix = new Matrix();

        matrix.setRotate( rotation, bitmap.getWidth() / 2, bitmap.getHeight() / 2 );
        matrix.postScale( scale, scale, bitmap.getWidth() / 2, bitmap.getHeight() / 2 );

        int newX = ( int ) ( MathFunc.convertX( pos.getX(), BitmapLibrary.getCanvas() ) - ( ( double ) bitmap.getWidth() / ( double ) 2 ) );
        int newY = ( int ) ( MathFunc.convertY( pos.getY(), BitmapLibrary.getCanvas() ) - ( ( double ) bitmap.getHeight() / ( double ) 2 ) );

        matrix.postTranslate( newX, newY );

        BitmapLibrary.getCanvas().drawBitmap( bitmap, matrix, new Paint() );
    }

    public static final void graphic( Bitmap bitmap, Vector2D pos,
                                      Vector2D pivotPoint, float scale, int rotation, int opacity )
    {
        Matrix matrix = new Matrix();

        matrix.setRotate( rotation, ( int ) pivotPoint.getX(), ( int ) pivotPoint.getY() );
        matrix.postScale( scale, scale, ( float ) pivotPoint.getX(), ( float ) pivotPoint.getY() );

        int newX = ( int ) ( MathFunc.convertX( pos.getX(), BitmapLibrary.getCanvas() ) - pivotPoint.getX() );
        int newY = ( int ) ( MathFunc.convertY( pos.getY(), BitmapLibrary.getCanvas() ) - pivotPoint.getY() );

        matrix.postTranslate( newX, newY );
        Paint paint = new Paint();
        paint.setAlpha( opacity );
        BitmapLibrary.getCanvas().drawBitmap( bitmap, matrix, paint );
    }

    public static final void drawTrackLine( Vector2D startPos, Vector2D endPos, int alpha )
    {
        Paint paint = new Paint();
        paint.setStyle( Paint.Style.FILL_AND_STROKE );

        int newStartX = MathFunc.convertX( startPos.getX(), BitmapLibrary.getCanvas() );
        int newStartY = MathFunc.convertY( startPos.getY(), BitmapLibrary.getCanvas() );
        int newEndX = MathFunc.convertX( endPos.getX(), BitmapLibrary.getCanvas() );
        int newEndY = MathFunc.convertY( endPos.getY(), BitmapLibrary.getCanvas() );

        paint.setStrokeWidth( 3 );
        paint.setColor( Color.rgb( 255, 255, 255 ) );
        paint.setAlpha( alpha );

        BitmapLibrary.getCanvas().drawLine( ( float ) newStartX, ( float ) newStartY, ( float ) newEndX, ( float ) newEndY, paint );

        paint.setStrokeWidth( 6 );
        paint.setAlpha( alpha / 2 );

        BitmapLibrary.getCanvas().drawLine( ( float ) newStartX, ( float ) newStartY, ( float ) newEndX, ( float ) newEndY, paint );

        paint.setStrokeWidth( 12 );
        paint.setAlpha( alpha / 4 );

        BitmapLibrary.getCanvas().drawLine( ( float ) newStartX, ( float ) newStartY, ( float ) newEndX, ( float ) newEndY, paint );
    }

    public static final void drawText( String text, Vector2D pos, int color )
    {
        Paint paint = new Paint();
        paint.setStyle( Paint.Style.FILL_AND_STROKE );

        paint.setColor( color );
        paint.setTextSize( MathFunc.convertX( 15, BitmapLibrary.getCanvas() ) );
        paint.setStrokeWidth( 1 );

        BitmapLibrary.getCanvas().drawText( text, ( float ) MathFunc.convertX( pos.getX(), BitmapLibrary.getCanvas() ), ( float ) MathFunc.convertY( pos.getY(), BitmapLibrary.getCanvas() ), paint );
    }

    @RequiresApi( api = Build.VERSION_CODES.LOLLIPOP )
    public static final void drawArc( Vector2D pos, int radius, int color, int startAngle,
                                      int sweepAngle, boolean useCenter )
    {
        Paint paint = new Paint();
        paint.setStyle( Paint.Style.FILL_AND_STROKE );
        paint.setColor( color );

        radius = MathFunc.convertX( radius, BitmapLibrary.getCanvas() );
        int newX = MathFunc.convertX( pos.getX(), BitmapLibrary.getCanvas() );
        int newY = MathFunc.convertY( pos.getY(), BitmapLibrary.getCanvas() );

        BitmapLibrary.getCanvas().drawArc( ( float ) newX - radius, ( float ) newY - radius,
                                           ( float ) newX + radius, ( float ) newY + radius,
                                           startAngle, sweepAngle,
                                           useCenter, paint );
    }
}
