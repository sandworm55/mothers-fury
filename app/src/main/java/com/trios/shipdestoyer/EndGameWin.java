package com.trios.shipdestoyer;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.warting.bubbles.ConversationActivity;

/**
 * Created by seand on 5/19/2017.
 */

public class EndGameWin extends Activity implements View.OnClickListener
{
    @Override
    protected void onCreate( Bundle savedInstance )
    {
        super.onCreate( savedInstance );
        setContentView( R.layout.activity_endgamewin );

        final Button btnSelect = ( Button ) findViewById( R.id.btnSelect );
        btnSelect.setOnClickListener( this );

        final Button btnNext = ( Button ) findViewById( R.id.btnNext );
        btnNext.setOnClickListener( this );


    }

    @Override
    public void onClick( View v )
    {
        Intent i;
        switch ( v.getId() )
        {
            case R.id.btnSelect:
                i = new Intent( this, Stage.class );
                break;
            case R.id.btnNext:
                i = new Intent( this, ConversationActivity.class );
                switch ( Level.levelType )
                {
                    case 1:
                        ConversationActivity.setConvoTwoPre();
                        LevelManager.currentLevel = LevelManager.LevelNum.LEVEL_TWO;
                        break;
                    case 2:
                        ConversationActivity.setConvoThreePre();
                        LevelManager.currentLevel = LevelManager.LevelNum.LEVEL_THREE;
                        break;
                    case 3:
                        ConversationActivity.setConvoFourPre();
                        LevelManager.currentLevel = LevelManager.LevelNum.LEVEL_FOUR;
                        break;
                    case 4:
                        ConversationActivity.setConvoFivePre();
                        LevelManager.currentLevel = LevelManager.LevelNum.LEVEL_FIVE;
                        break;
                    case 5:
                        ConversationActivity.setConvoSixPre();
                        LevelManager.currentLevel = LevelManager.LevelNum.LEVEL_SIX;
                        break;
                    case 6:
                        ConversationActivity.setConvoSevenPre();
                        LevelManager.currentLevel = LevelManager.LevelNum.LEVEL_SEVEN;
                        break;
                    case 7:
                        ConversationActivity.setConvoEightPre();
                        LevelManager.currentLevel = LevelManager.LevelNum.LEVEL_EIGHT;
                        break;
                    case 8:
                        ConversationActivity.setConvoNinePre();
                        LevelManager.currentLevel = LevelManager.LevelNum.LEVEL_NINE;
                        break;
                    case 9:
                        ConversationActivity.setConvoTenPre();
                        LevelManager.currentLevel = LevelManager.LevelNum.LEVEL_TEN;
                        break;
                    case 10:
                        ConversationActivity.setConvoElevenPre();
                        LevelManager.currentLevel = LevelManager.LevelNum.LEVEL_ELEVEN;
                        break;
                    case 11:
                        ConversationActivity.setConvoTwelvePre();
                        LevelManager.currentLevel = LevelManager.LevelNum.LEVEL_TWELVE;
                        break;
                    case 12:
                        ConversationActivity.setConvoThirteenPre();
                        LevelManager.currentLevel = LevelManager.LevelNum.LEVEL_THIRTEEN;
                        break;
                    case 13:
                        ConversationActivity.setConvoFourteenPre();
                        LevelManager.currentLevel = LevelManager.LevelNum.LEVEL_FOURTEEN;
                        break;
                    case 14:
                        ConversationActivity.setConvoFifteenPre();
                        LevelManager.currentLevel = LevelManager.LevelNum.LEVEL_FIFTEEN;
                        break;
                }
                break;
            default:
                i = null;
        }
        startActivity( i );
        finish();
    }
}
