package com.trios.shipdestoyer;

import android.graphics.Bitmap;

import com.trios.shipdestoyer.lib.DrawUtil;
import com.trios.shipdestoyer.lib.SoundManager;
import com.trios.shipdestoyer.lib.Vector2D;

import java.util.Random;

/**
 * Created by sandworm on 5/16/2017.
 */

public class HitEffect
{
    final int SPEED = 4;
    Bitmap bitmap;
    float scale;
    int opacity = 255;
    int value = 0;
    Vector2D pos;
    Type type;

    public HitEffect( Type type, Vector2D pos )
    {
        this.type = type;
        Random rand = new Random();
        this.pos = pos;

        if ( type == Type.EXPLOSION )
            this.pos = this.pos.add( new Vector2D( rand.nextInt( 100 ) - 50, rand.nextInt( 60 ) - 30 ) );

        switch ( type )
        {
            case SPARK:
                SoundManager.playSound( SoundManager.SHIELD_HIT, 0, 0, 1 );
                bitmap = BitmapLibrary.getBitmap( BitmapLibrary.BitmapFile.SHIELD_SPARKS );
                break;
            case EXPLOSION:
                SoundManager.playSound( SoundManager.EXPLOSION, 0, 0, 1 );
                bitmap = BitmapLibrary.getBitmap( BitmapLibrary.BitmapFile.SMALL_EXPLOSION );
                break;
            default:
                bitmap = BitmapLibrary.getBitmap( BitmapLibrary.BitmapFile.CRUISER );
        }
    }

    public void draw()
    {
        Random rand = new Random();
        if ( type == Type.SPARK )
            DrawUtil.graphic( bitmap, pos, new Vector2D( bitmap.getWidth() / 2, 0 + ( ( scale / 4f ) ) * bitmap.getHeight() ), scale, 0, opacity );
        else if ( type == Type.EXPLOSION )
            DrawUtil.graphic( bitmap, pos, new Vector2D( bitmap.getWidth() / 2, bitmap.getHeight() / 2 ), scale + rand.nextFloat(), 0, opacity );

    }

    public void update()
    {
        if ( type == Type.SPARK )
            value += SPEED;
        value += SPEED;
        if ( value >= 255 )
            value = 255;

        scale = ( ( float ) value / 255 ) * 2;
        if ( type == Type.SPARK )
            scale = scale * 2;
        opacity = 255 - value;
    }

    public boolean isDone()
    {
        return opacity == 0;
    }

    public enum Type
    {
        SPARK, EXPLOSION
    }
}
