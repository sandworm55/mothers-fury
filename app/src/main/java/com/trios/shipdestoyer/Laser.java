package com.trios.shipdestoyer;

import android.graphics.Bitmap;

import com.trios.shipdestoyer.lib.DrawUtil;
import com.trios.shipdestoyer.lib.SoundManager;
import com.trios.shipdestoyer.lib.Vector2D;

/**
 * Created by sandworm on 5/15/2017.
 */

public class Laser extends Turret
{
    Bitmap bitmapTurret;
    Bitmap bitmapFlash;
    Bitmap laser;
    private int barrelLength;
    private float soundSpeed;

    /**
     * @param type
     * @param facing
     */
    public Laser( TurretTypes type, int facing )
    {
        super( type, facing );
        laser = BitmapLibrary.getBitmap( BitmapLibrary.BitmapFile.LASER );
        switch ( type )
        {
            case SMALL_LASER:
                bitmapTurret = BitmapLibrary.getBitmap( BitmapLibrary.BitmapFile.SMALL_LASER_TURRET );
                bitmapFlash = BitmapLibrary.getBitmap( BitmapLibrary.BitmapFile.SMALL_LASER_FLASH );
                rotationSpeed = 1;
                projectileSpeed = 3;
                offSet.add( 0 );
                FIRE_RATE = 60;
                soundSpeed = 2f;
                damage = 3;
                break;
            case MEDIUM_LASER:
                bitmapTurret = BitmapLibrary.getBitmap( BitmapLibrary.BitmapFile.MEDIUM_LASER_TURRET );
                bitmapFlash = BitmapLibrary.getBitmap( BitmapLibrary.BitmapFile.MEDIUM_LASER_FLASH );
                rotationSpeed = .5f;
                projectileSpeed = 3;
                offSet.add( -10 );
                offSet.add( 10 );
                FIRE_RATE = 100;
                soundSpeed = 1.8f;
                damage = 2;
                break;
            case LARGE_LASER:
                bitmapTurret = BitmapLibrary.getBitmap( BitmapLibrary.BitmapFile.LARGE_LASER_TURRET );
                bitmapFlash = BitmapLibrary.getBitmap( BitmapLibrary.BitmapFile.LARGE_LASER_FLASH );
                rotationSpeed = .05f;
                projectileSpeed = 3;
                offSet.add( -17 );
                offSet.add( 0 );
                offSet.add( 17 );
                FIRE_RATE = 140;
                soundSpeed = 0.85f;
                damage = 2;
                break;
            case MASSIVE_LASER:
                bitmapTurret = BitmapLibrary.getBitmap( BitmapLibrary.BitmapFile.MASSIVE_LASER_TURRET );
                bitmapFlash = BitmapLibrary.getBitmap( BitmapLibrary.BitmapFile.MASSIVE_LASER_FLASH );
                rotationSpeed = .05f;
                projectileSpeed = 3;
                offSet.add( -30 );
                offSet.add( -15 );
                offSet.add( 15 );
                offSet.add( 30 );
                FIRE_RATE = 180;
                soundSpeed = 0.6f;
                damage = 3;
                break;
            default:
                bitmapTurret = BitmapLibrary.getBitmap( BitmapLibrary.BitmapFile.SMALL_EXPLOSION );
                bitmapFlash = BitmapLibrary.getBitmap( BitmapLibrary.BitmapFile.SHIELD_SPARKS );
                damage = 100;
        }
        barrelLength = bitmapTurret.getHeight() / 2;
    }

    public void draw()
    {
        super.draw();

        DrawUtil.graphic( bitmapTurret, pos, ( int ) facing );
        int opacity;
        opacity = ( currentFireRate - ( FIRE_RATE / 2 ) );
        if ( opacity <= 0 )
            opacity = 0;
        else
            opacity = ( int ) ( ( ( double ) opacity / ( ( double ) FIRE_RATE / ( double ) 2 ) ) * ( double ) 255 );


        DrawUtil.graphic( bitmapFlash, pos, ( int ) facing, opacity );

        if ( currentFireRate - ( FIRE_RATE / 2 ) == 0 )
        {
            SoundManager.playSound( SoundManager.LASER_CHARGING, 0, 0, soundSpeed );
        }
    }

    @Override
    public Boolean fireBullet()
    {
        if ( !lockedOn )
            return false;
        currentFireRate = 0;
        for ( int shot :
                offSet )
        {
            double disX = ( Math.sin( Math.toRadians( facing ) ) * shot );
            double disY = ( Math.cos( Math.toRadians( facing ) ) * shot );

            double barX = ( Math.sin( Math.toRadians( facing ) ) * barrelLength );
            double barY = ( Math.cos( Math.toRadians( facing ) ) * barrelLength );
            projectiles.add( new Projectile( this.getPos().add( new Vector2D( disY, disX ) ).add( new Vector2D( -barX, barY ) ),
                                             target, projectileSpeed, this.getType(), damage ) );
        }

        return true;
    }
}
